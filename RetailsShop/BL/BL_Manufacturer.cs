﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;

namespace RetailsShop.BL
{
    class BL_Manufacturer
    {

        DL_Manufacturer dl_manufacture = new DL_Manufacturer();

        private string manufactureid;
        private string manufacturediscri;
        private string manufacture;


        public string ManufactureId
        {
            get { return manufactureid; }
            set { manufactureid = value; }
        }


        public string ManufactureDiscri
        {
            get { return manufacturediscri; }
            set { manufacturediscri = value; }
        }


        public string ManufactureName
        {
            get { return manufacture; }
            set { manufacture = value; }
        }

        public void InsertManufacturer(BL_Manufacturer bl_manufacture)
        {
            dl_manufacture.InsertManufacturer(bl_manufacture);
        }

        public void UpdateManufacturer(BL_Manufacturer bl_manufacture)
        {
            dl_manufacture.UpdateManufacturer(bl_manufacture);
        }


        public void DeleteManufacturer(BL_Manufacturer bl_manufacture)
        {
            dl_manufacture.DeleteManufacturer(bl_manufacture);
        }


        public DataTable SelectManufacturer()
        {
            DataTable dt = dl_manufacture.SelectManufacturer();
            return dt;
        }


    }
}
