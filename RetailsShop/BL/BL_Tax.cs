﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;

namespace RetailsShop.BL
{
    class BL_Tax
    {
        DL_Tax dl_tax = new DL_Tax();
        //tax
        private string taxid;
        private string taxdes;
        private string taxrate;


        public string TaxId
        {
            get { return taxid; }
            set { taxid = value; }
        }
        public string TaxDescription
        {
            get { return taxdes; }
            set { taxdes = value; }
        }
        public string TaxRate
        {
            get { return taxrate; }
            set { taxrate = value; }
        }

        public void InsertTax(BL_Tax bl_tax)
        {
            dl_tax.InsertTax(bl_tax);
        }

        public void UpdateTax(BL_Tax bl_tax)
        {
            dl_tax.UpdateTax(bl_tax);
        }
        public void DeleteTax(BL_Tax bl_tax)
        {
            dl_tax.DeleteTax(bl_tax);
        }
        public DataTable SelectTax()
        {
            DataTable dt = dl_tax.SelectTax();
            return dt;
        }


    }
}
