﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RetailsShop.PL;
using RetailsShop.BL;
using RetailsShop.DL;

using System.Data;
using System.Data.SqlClient;

namespace RetailsShop.BL
{
    class BL_SendSMS
    {

        DL_SendSMS dl_sendsms = new DL_SendSMS();

        public DataTable SelectSendSms()
        {
            DataTable dt = dl_sendsms.SelectSendSms();
            return dt;
        }

        public void InsertSendSms(BL_Field bl_field)
        {
            dl_sendsms.InsertSendSms(bl_field);
        }

        public void UpdateSendSms(BL_Field bl_field)
        {
            dl_sendsms.UpdateSendSms(bl_field);
        }

        public void DeleteSendSms(BL_Field bl_field)
        {
            dl_sendsms.DeleteSendSms(bl_field);
        }
    }
}
