﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;

namespace RetailsShop.BL
{
    class BL_TreeView
    {

        //catagory
        DL_TreeView dl_treeview= new DL_TreeView();

        private int menuid;
        private string menuname;
        private string itemName;
        private string itemid;
        private string status;
        //treeview

        public int MenuId
        {
            get { return menuid; }
            set { menuid = value; }
        }

        public string MenuName
        {
            get { return menuname; }
            set { menuname = value; }
        }
        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        }
        public string ItemId
        {
            get { return itemid; }
            set { itemid = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }


        public void InsertTreeViewMenu(BL_TreeView bl_treeview)
        {
            dl_treeview.InsertTreeViewMenu(bl_treeview);
        }

        public DataTable SelectTreeViewMenu(BL_TreeView bl_treeview)
        {
            DataTable dt = dl_treeview.SelectTreeViewMenu(bl_treeview);
            return dt;
        }

        public void UpdateTreeViewMenu(BL_TreeView bl_treeview)
        {
            dl_treeview.UpdateTreeViewMenu(bl_treeview);
        }
        public void DeleteTreeViewMenu(BL_TreeView bl_treeview)
        {
            dl_treeview.DeleteTreeViewMenu(bl_treeview);
        }

        //subcatagory

        public void InsertTreeViewItem(BL_TreeView bl_treeview)
        {
            dl_treeview.InsertTreeViewItem(bl_treeview);
        }

        public DataTable SelectTreeViewItem(string cataid)
        {
            DataTable dt = dl_treeview.SelectTreeViewItem(cataid);
            return dt;
        }

        public void UpdateTreeViewItem(BL_TreeView bl_treeview)
        {
            dl_treeview.UpdateTreeViewItem(bl_treeview);
        }

        public void DeleteTreeViewItem(BL_TreeView bl_treeview)
        {
            dl_treeview.DeleteTreeViewItem(bl_treeview);
        }

        public void TreeViewItemChange(BL_TreeView bl_treeview)
        {
            dl_treeview.TreeViewItemChange(bl_treeview);
        }


        public DataTable SelectTreeViewItem()
        {
            DataTable dt = dl_treeview.SelectTreeViewItem();
            return dt;
        }

    }
}
