﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;



namespace RetailsShop.BL
{
    class BL_Worker
    {
        DL_Worker dl_worker = new DL_Worker();

        public void InsertWorker(BL_Field bl_field)
        {
            dl_worker.InsertWorker(bl_field);
        }

        public DataTable SelectWorker()
        {
            DataTable dt = dl_worker.SelectWorker();
            return dt;
        }
        public DataTable SelectWaiter(BL_Field bl_field)
        {
            DataTable dt = dl_worker.SelectWaiter(bl_field);
            return dt;
        }
        public DataTable SelectWaiterName(BL_Field bl_field)
        {
            DataTable dt = dl_worker.SelectWaiterName(bl_field);
            return dt;
        }
        public DataTable SelectCashier()
        {
            DataTable dt = dl_worker.SelectCashier();
            return dt;
        }
        public DataTable CheckUsername(BL_Field bl_field)
        {
            DataTable dt = dl_worker.CheckUsername(bl_field);
            return dt;
        }
        public void UpdateWorker(BL_Field bl_field)
        {
            dl_worker.UpdateWorker(bl_field);
        }
        public void DeleteWorker(BL_Field bl_field)
        {
            dl_worker.DeleteWorker(bl_field);
        }

        public SqlDataReader CheckLogin(BL_Field bl_field)
        {
          SqlDataReader dr=  dl_worker.CheckLogin(bl_field);
          return dr;
        }


        public SqlDataReader CheckSecurity(BL_Field bl_field)
        {
            SqlDataReader dr = dl_worker.CheckSecurity(bl_field);
            return dr;
        }


    }
}
