﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RetailsShop.PL;
using RetailsShop.BL;
using RetailsShop.DL;

using System.Data;
using System.Data.SqlClient;

namespace RetailsShop.BL
{
    class BL_OrganisationDetails
    {
               
        DL_OrganisationDetails dl_organisationdetails = new DL_OrganisationDetails();


        public void InsertOrganisationDetails(BL_Field bl_field)
        {
            dl_organisationdetails.InsertOrganisationDetails(bl_field);
        }

        public void UpdateOrganisationDetails(BL_Field bl_field)
        {
            dl_organisationdetails.UpdateOrganisationDetails(bl_field);
        }


        public void DeleteOrganisationDetails(BL_Field bl_field)
        {
            dl_organisationdetails.DeleteOrganisationDetails(bl_field);
        }


        public DataTable SelectOrganisationDetails()
        {
            DataTable dt = dl_organisationdetails.SelectOrganisationDetails();
            return dt;


        }
    }
}
