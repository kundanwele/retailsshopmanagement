﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;


namespace RetailsShop.BL
{
  class BL_Vendor
    {



        DL_Vendor dl_vendor = new DL_Vendor();


        //supplier
        private string supplierid;
        private string suppliername;
        private string contactperson;
        private string note;
        private string phoneno;
        private string mobileno;
        private string email;
        private string country;
        private string state;
        private string city;
        private string address;
        private string textsearch;


        //supplier
        public string SupplierId
        {
            get { return supplierid; }
            set { supplierid = value; }
        }

        public string SupplierName
        {
            get { return suppliername; }
            set { suppliername = value; }
        }
        public string ContactPerson
        {
            get { return contactperson; }
            set { contactperson = value; }
        }
        public string Note
        {
            get { return note; }
            set { note = value; }
        }

        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        public string State
        {
            get { return state; }
            set { state = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        public string PhoneNo
        {
            get { return phoneno; }
            set { phoneno = value; }
        }


        public string Mobile
        {
            get { return mobileno; }
            set { mobileno = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string TextSearch
        {
            get { return textsearch; }
            set { textsearch = value; }
        }



        public void InsertVendor(BL_Vendor bl_vender)
        {
            dl_vendor.InsertVendor(bl_vender);
        }

        public void UpdateVendor(BL_Vendor bl_vender)
        {
            dl_vendor.UpdateVendor(bl_vender);
        }
        public void DeleteVendor(BL_Vendor bl_vender)
        {
            dl_vendor.DeleteVendor(bl_vender);
        }
        public DataTable SelectVendor()
        {
            DataTable dt = dl_vendor.SelectVendor();
            return dt;
        }

        public DataTable SearchVendor(BL_Vendor bl_vender)
        {
            DataTable dt = dl_vendor.SearchVendor(bl_vender);
            return dt;
        }
    }
}
