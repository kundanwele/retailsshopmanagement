﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RetailsShop.PL;
using RetailsShop.DL;
using RetailsShop.BL;

using System.Data;
using System.Data.SqlClient;



namespace RetailsShop.BL
{
    class BL_OtherExpences
    {
        DL_OtherExpences dl_otherexpences = new DL_OtherExpences();

        private string expensename;
        private string amount;
        private string remark;
        private string otherexpenseid; 

        public string ExpenseName
        {
            get { return expensename; }
            set { expensename = value; }

        }


        public string Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public string Remark

        {
            get { return remark ; }
            set { remark = value;}
        } 

        public string OtherExpenseId 

        {
            get { return otherexpenseid; }
            set { otherexpenseid = value; }
        }  

        public DataTable SelectOtherExpences()
        {
            DataTable dt = dl_otherexpences.SelectOtherExpences();
            return dt;
        }


        public void InsertOtherExpences(BL_Field bl_field)
        {
            dl_otherexpences.InsertOtherExpences(bl_field);
        }


        public void UpdateOtherExpences(BL_Field bl_field)
        {
            dl_otherexpences.UpdateOtherExpences(bl_field);
        }


        public void DeleteOtherExpences(BL_Field bl_field)
        {
            dl_otherexpences.DeleteOtherExpences(bl_field);
        }


        

    }
}
