﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using RetailsShop.BL;
using RetailsShop.PL;
using RetailsShop.DL;

using System.Data;
using System.Data.SqlClient;



namespace RetailsShop.BL
{
    class BL_AddPaymentMode
    {
        DL_AddPaymentMode dl_addpaymentmode = new DL_AddPaymentMode();


        private string paymentmode;
        private string paymentmodeid;


        public string PaymentMode
        {
            get { return paymentmode; }
            set { paymentmode = value; }
        }

        public string PaymentModeId
        {
            get { return paymentmodeid; }
            set { paymentmodeid = value; }
        }



        public DataTable SelectPaymentMode()
        {

            DataTable dt = dl_addpaymentmode.SelectPaymentMode();
            return dt;

        }


        public void InsertPaymentMode(BL_Field bl_field)
        {

            
            dl_addpaymentmode.InsertPaymentMode( bl_field);

        }


        public void UpdatePaymentMode(BL_Field bl_field)
        {
            
            dl_addpaymentmode.UpdatePaymentMode( bl_field);

        }

        public void DeletePaymentMode(BL_Field bl_field)
        {
           dl_addpaymentmode.DeletePaymentMode( bl_field);

        }


    }
}
