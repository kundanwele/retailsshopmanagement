﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;


namespace RetailsShop.BL
{
    class BL_Sale
    {

        private string orderid;
        private string orderdeatilid;
        private string instalid;
        private string instid;
        private string instdetailid;
        private string productid;
        private string customerid;
        private string qty;
        private string rate;
        private string amount;
        private string paydate;
        private string noofinst;
        private string nextpaydate;
        private string price;
        private string textsearch;
        private string invoiceno;
        private string vat;
        private string vatamount;
        private string date;
        private string discount;
         private string serialno;
        private string status;










        public string OrderId
        {
            get { return orderid; }
            set { orderid = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public string OrderDetailId
        {
            get { return orderdeatilid; }
            set { orderdeatilid = value; }
        }

        public string InstalId
        {
            get { return instalid; }
            set { instalid = value; }
        }

        public string InstDetailId
        {
            get { return instdetailid; }
            set { instdetailid = value; }
        }

        public string ProductId
        {
            get { return productid; }
            set { productid = value; }
        }

        public string CustomerId
        {
            get { return customerid; }
            set { customerid = value; }
        }


        public string Qty
        {
            get { return qty; }
            set { qty = value; }
        }

        public string Rate
        {
            get { return rate; }
            set { rate = value; }
        }

        public string Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public string PayDate
        {
            get { return paydate; }
            set { paydate = value; }
        }

        public string NoOfInst
        {
            get { return noofinst; }
            set { noofinst = value; }
        }


        public string NextPayDate
        {
            get { return nextpaydate; }
            set { nextpaydate = value; }
        }

        public string Price
        {
            get { return price; }
            set { price = value; }
        }

        public string TextSearch
        {
            get { return textsearch; }
            set { textsearch = value; }
        }

        public string InvoiceNo
        {
            get { return invoiceno; }
            set { invoiceno = value; }
        }


        public string Vat
        {
            get { return vat; }
            set { vat = value; }
        }

        public string VatAmount
        {
            get { return vatamount; }
            set { vatamount = value; }
        }

        public string Date
        {
            get { return date; }
            set { date = value; }
        }


        public string Discount
        {
            get { return discount; }
            set { discount = value; }
        }

        public string SerialNo
        {
            get { return serialno; }
            set { serialno = value; }
        }

       



        DL_Sale dl_sale = new DL_Sale();


        public DataTable SelectCustomer(BL_Sale bl_sale)
        {
            DataTable dt = dl_sale.SelectCustomer(bl_sale);
            return dt;
        }

        public DataTable SelectCustomerAll()
        {
            DataTable dt = dl_sale.SelectCustomerAll();
            return dt;
        }

        //public string InsertOrder(BL_Sale bl_sale)
        //{
        //    dl_sale.InsertOrder(bl_sale);
            
        //}

        public void InsertOrderDetails(BL_Sale bl_sale)
        {
            dl_sale.InsertOrderDetails(bl_sale);

        }

        public DataSet SelectOrderDetails(BL_Sale bl_sale)
        {
            DataSet dt = dl_sale.SelectOrderDetails(bl_sale);
            return dt;
        }

        public void DeleteOrderDetails(BL_Sale bl_sale)
        {
            dl_sale.DeleteOrderDetails(bl_sale);

        }

        public void UpdateOrderDetails(BL_Sale bl_sale)
        {
            dl_sale.UpdateOrderDetails(bl_sale);

        }

        public string InsertOrder(BL_Sale bl_sale)
        {
           string id =dl_sale.InsertOrder(bl_sale);
           return id;
        }




        
    }
}
