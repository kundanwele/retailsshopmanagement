﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


using RetailsShop.PL;
using RetailsShop.BL;
using RetailsShop.DL;


using System.Data.SqlClient;

namespace RetailsShop.BL
{
    class BL_PrinterSetup
    {
        DL_PrinterSetup dl_PrinterSetup = new DL_PrinterSetup();
        private string billprinter;
        private string kotprinter;
        
        public string BillPrinter
        {
            get { return billprinter; }
            set { billprinter = value; }
        }
        public string KotPrinter
        {
            get { return kotprinter; }
            set { kotprinter = value; }
        }

        //Printer 

        public void InsertPrinter(BL_PrinterSetup bl_PrinterSetup)
        {
            dl_PrinterSetup.InsertPrinter(bl_PrinterSetup);
        }

        public void UpdatePrinter(BL_PrinterSetup bl_PrinterSetup)
        {
            dl_PrinterSetup.UpdatePrinter(bl_PrinterSetup);
        }

        public DataTable SelectPrinter()
        {
            DataTable dt = dl_PrinterSetup.SelectPrinter();
            return dt;
        }




    }
}
