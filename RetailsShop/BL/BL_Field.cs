﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;


namespace RetailsShop.BL
{
    class BL_Field
    {


        //worker
        private string workerid;
        private string name;
        private string middlename;
        private string lastname;
        private string phoneno;
        private string mobileno;
        private string email;
        private string country;
        private string state;
        private string city;
        private string address;
        private string role;
        private static string rolelogin;
        private string username;
        private string password;
       

        private string date;
        private string time;

    

        private string status;
        private static string fromdate;
        private static string todate;
        private static int receiptno;
        private string price;
        private string qty;
        private string billno;
        private string saleId;


        private string paymentmode;
        private string paymentmodeid;


        private string officeno;
        private string shopid;

        private string retypepass;

        private string expensename;
        private string amount;
        private string remark;
        private string otherexpenseid;

        private string sendto;
        private string message;
        private string sendsmsid;








        public string WorkerId
        {
            get { return workerid; }
            set { workerid = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }



        public string MiddleName
        {
            get { return middlename; }
            set { middlename = value; }
        }

        public string LastName
        {
            get { return lastname; }
            set { lastname = value; }
        }
        public string PhoneNo
        {
            get { return phoneno; }
            set { phoneno = value; }
        }


        public string Mobile
        {
            get { return mobileno; }
            set { mobileno = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }


        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        public string State
        {
            get { return state; }
            set { state = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        public string Role
        {
            get { return role; }
            set { role = value; }
        }

        public string RoleLogin
        {
            get { return rolelogin; }
            set { rolelogin = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string ReTypePass
        {
            get { return retypepass; }
            set { retypepass = value; }
        }



        public string Date
        {
            get { return date; }
            set { date = value; }
        }

        public string Time
        {
            get { return time; }
            set { time = value; }
        }


        public string FromDate
        {
            get { return fromdate; }
            set { fromdate = value; }
        }
        public string Todate
        {
            get { return todate; }
            set { todate = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public int ReceiptNo
        {
            get { return receiptno; }
            set { receiptno = value; }
        }

        public string Price
        {
            get { return price; }
            set { price = value; }
        }


        public string Qty
        {
            get { return qty; }
            set { qty = value; }
        }

 

        public string SaleId
        {
            get { return saleId; }
            set { saleId = value; }
        }
        public string BillNo
        {
            get { return billno; }
            set { billno = value; }
        }

        public string PaymentMode
        {
            get { return paymentmode; }
            set { paymentmode = value; }
        }

        public string PaymentModeId
        {
            get { return paymentmodeid; }
            set { paymentmodeid = value; }
        }

        public string OfficeNo
        {
            get { return officeno; }
            set { officeno = value; }
        }


        public string ShopId
        {
            get { return shopid; }
            set { shopid = value; }
        }

       
        public string ExpenseName
        {
            get { return expensename; }
            set { expensename = value; }

        }


         public string Amount
        {
            get { return amount; }
            set { amount = value; }
        }


         public string Remark
         {
             get { return remark; }
             set { remark = value; }
         }

         public string OtherExpenseId
         {
             get { return otherexpenseid; }
             set { otherexpenseid = value; }
         }


         public string SendTo
         {
             get { return sendto; }
             set {sendto= value; }
         }


         public string Message
         {
             get { return message; }
             set { message = value; }
         }


         public string SendSmsId
         {
             get { return sendsmsid; }
             set { sendsmsid = value; }
         }  




        //common thing added here
         private string textsearch;
         private string orgname="Nihal Electronics";
         private string orgaddress="Bhalar Town Ship";



         public string TextSearch
         {
             get { return textsearch; }
             set { textsearch = value; }
         }

         public string OrgName
         {
             get { return orgname; }
             set { orgname = value; }
         }
         public string OrgAddress
         {
             get { return orgaddress; }
             set { orgaddress = value; }
         }
        
    }
}
