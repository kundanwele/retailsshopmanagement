﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;


namespace RetailsShop.BL
{
    class BL_Purchase
    {

       
        DL_Purchase dl_purchase = new DL_Purchase();

        //purchase

       
      
        private string productnameId;
        private string productname;

        private string serialno;
        private string vat;
     
        private string qty;
        private string date;
        private string rate;
       
        
      
        private string invoiceno;
        private string status;
       
        private string supplierid;
        private string manufactureid;

        private string discount;
        private string textsearch;
        private int purchaseid;
        private int purchasedetailid;
       


        
        public string ProductNameId
        {
            get { return productnameId; }
            set { productnameId = value; }
        }

        public string ProductName
        {
            get { return productname; }
            set { productname = value; }
        }
       
       
        public string Rate
        {
            get { return rate; }
            set { rate = value; }
        }

        public string SerialNo
        {
            get { return serialno; }
            set { serialno = value; }
        }
       
       
        

        public string Vat
        {
            get { return vat; }
            set { vat = value; }
        }
       

        public string Qty
        {
            get { return qty; }
            set { qty = value; }
        }

        public string Date
        {
            get { return date; }
            set { date = value; }
        }

        public string Discount
        {
            get { return discount; }
            set { discount = value; }
        }
        
        
        
        public string InvoiceNo
        {
            get { return invoiceno; }
            set { invoiceno = value; }
        }
      
        public string SupplierId
        {
            get { return supplierid; }
            set { supplierid = value; }
        }
       
        public string TextSearch
        {
            get { return textsearch; }
            set { textsearch = value; }
        }

        public int PurchaseId
        {
            get { return purchaseid; }
            set { purchaseid = value; }
        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        }



        public string ManufactureId
        {
            get { return manufactureid; }
            set { manufactureid = value; }
        }

        public int PurchaseDetailId
        {
            get { return purchasedetailid; }
            set { purchasedetailid = value; }
        }
       
       
      
      


        //public string InsertPurchase(BL_Purchase bl_purchase)
        //{
        // string id=dl_purchase.InsertPurchase(bl_purchase);
        // return id;
        //}

        public void InsertPurchaseDetail(BL_Purchase bl_purchase)
        {
           dl_purchase.InsertPurchaseDetail(bl_purchase);
           
        }

        public DataTable SelectPurchaseDetail(BL_Purchase bl_purchase)
        {
            DataTable dt = dl_purchase.SelectPurchaseDetail(bl_purchase);
            return dt;
        }

        public void UpdatePurchaseDetail(BL_Purchase bl_purchase)
        {
            dl_purchase.UpdatePurchaseDetail(bl_purchase);
        }

        public void DeletePurchaseDetail(BL_Purchase bl_purchase)
        {
            dl_purchase.DeletePurchaseDetail(bl_purchase);
        }

        //public DataTable SelectPurchaseByDate(BL_Purchase bl_purchase)
        //{
        //    DataTable dt = dl_purchase.SelectPurchaseByDate(bl_purchase);
        //    return dt;
        //}
        //public DataTable SelectProductNameBySearch(BL_Purchase bl_purchase)
        //{
        //    DataTable dt = dl_purchase.SelectProductNameBySearch(bl_purchase);
        //    return dt;
        //}

        //public void ClosePurchase()
        //{
        //    dl_purchase.ClosePurchase();
        //}



        //public DataTable SelectPurchaseById(BL_Purchase bl_purchase)
        //{
        //    DataTable dt = dl_purchase.SelectPurchaseById(bl_purchase);
        //    return dt;
        //}


        public DataTable SelectManufact()
        {
            DataTable dt = dl_purchase.SelectManufact();
            return dt;
        }

        public void InsertPurchase(BL_Purchase bl_purchase)
        {
            dl_purchase.InsertPurchase(bl_purchase);

        }


        public DataTable SelectProduct(BL_Purchase bl_purchase)
        {
            DataTable dt = dl_purchase.SelectProduct(bl_purchase);
            return dt;
        }

        public DataTable SelectAllProduct()
        {
            DataTable dt = dl_purchase.SelectAllProduct();
            return dt;
        }
    }
}
