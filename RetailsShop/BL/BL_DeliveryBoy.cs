﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;


namespace RetailsShop.BL
{
    class BL_DeliveryBoy
    {
        DL_DeliveryBoy dl_deliveryboy = new DL_DeliveryBoy();

        private string deliveryboysid;
        private string name;
        private string middlename;
        private string lastname;
        private string phoneno;
        private string mobileno;
        private string email;
        private string country;
        private string state;
        private string city;
        private string address;
       

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string MiddleName
        {
            get { return middlename; }
            set { middlename = value; }
        }

        public string LastName
        {
            get { return lastname; }
            set { lastname = value; }
        }
        public string PhoneNo
        {
            get { return phoneno; }
            set { phoneno = value; }
        }


        public string Mobile
        {
            get { return mobileno; }
            set { mobileno = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }


        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        public string State
        {
            get { return state; }
            set { state = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public string DeliveryBoysId
        {
            get { return deliveryboysid; }
            set { deliveryboysid = value; }
        }

      

        public void InsertDeliveryBoy(BL_DeliveryBoy bl_deliveryboy)
        {
            dl_deliveryboy.InsertDeliveryBoy(bl_deliveryboy);
        }

        public DataTable SelectDeliveryBoy()
        {
            DataTable dt = dl_deliveryboy.SelectDeliveryBoy();
            return dt;
        }

        public void UpdateDeliveryBoy(BL_DeliveryBoy bl_deliveryboy)
        {
            dl_deliveryboy.UpdateDeliveryBoy(bl_deliveryboy);
        }


        public void DeleteDeliveryBoy(BL_DeliveryBoy bl_deliveryboy)
        {
            dl_deliveryboy.DeleteDeliveryBoy(bl_deliveryboy);
        }





    }
}
