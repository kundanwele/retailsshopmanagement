﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;


namespace RetailsShop.BL
{
    class BL_Manager
    {



        private string name;
        private string middlename;
        private string lastname;
        private string phoneno;
        private string mobileno;
        private string email;
        private string country;
        private string state;
        private string city;
        private string address;
        private string managerid;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string MiddleName
        {
            get { return middlename; }
            set { middlename = value; }
        }

        public string LastName
        {
            get { return lastname; }
            set { lastname = value; }
        }
        public string PhoneNo
        {
            get { return phoneno; }
            set { phoneno = value; }
        }


        public string Mobile
        {
            get { return mobileno; }
            set { mobileno = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }


        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        public string State
        {
            get { return state; }
            set { state = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public string ManagerId
        {
            get { return managerid; }
            set { managerid = value; }
        }


        DL_Manager dl_manager = new DL_Manager();


        public void InsertManager(BL_Manager bl_manager)
        {
            dl_manager.InsertManager(bl_manager);
        }

        public DataTable SelectManager()
        {
            DataTable dt = dl_manager.SelectManager();
            return dt;
        }

        public void UpdateManager(BL_Manager bl_manager)
        {
            dl_manager.UpdateManager(bl_manager);
        }

        public void DeleteManager(BL_Manager bl_manager)
        {
            dl_manager.DeleteManager(bl_manager);
        }




    }
}
