﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RetailsShop.PL;
using RetailsShop.BL;
using RetailsShop.DL;

using System.Data;
using System.Data.SqlClient;


namespace RetailsShop.BL
{
    class BL_SMSsetting
    {

        
        DL_SMSsetting dl_smssetting = new DL_SMSsetting();
       

        private string url;
        private string userid;
        private string password;
        private string smsid;
        private string host;
        private string port;


        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        public string UserId
        {
            get { return userid; }
            set { userid = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string SmsId
        {
            get { return smsid; }
            set { smsid = value; }
        }

        //email
        private string email;
        private string emailid;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

  

        public string EmailId
        {
            get { return emailid; }
            set { emailid = value; }
        }

        public string Host
        {
            get { return host; }
            set { host = value; }
        }
        public string Port
        {
            get { return port; }
            set { port = value; }
        }



        public void InsertSmsSetting(BL_SMSsetting bl_smssetting)
        {
            dl_smssetting.InsertSmsSetting(bl_smssetting);
        }

        public void UpdateSmsSetting(BL_SMSsetting bl_smssetting)
        {
            dl_smssetting.UpdateSmsSetting(bl_smssetting);
        }


        public void DeleteSmsSetting(BL_SMSsetting bl_smssetting)
        {
            dl_smssetting.DeleteSmsSetting(bl_smssetting);
        }


        public DataTable SelectSmsSetting()
        {
            DataTable dt = dl_smssetting.SelectSmsSetting();
            return dt;


        }



        //emails
        public void Insertemail(BL_SMSsetting bl_smssetting)
        {
            dl_smssetting.Insertemail(bl_smssetting);
        }

        public void Updateemail(BL_SMSsetting bl_smssetting)
        {
            dl_smssetting.Updateemail(bl_smssetting);
        }
        public void Deleteemail(BL_SMSsetting bl_smssetting)
        {
            dl_smssetting.Deleteemail(bl_smssetting);
        }
        public DataTable Selectemail()
        {
            DataTable dt = dl_smssetting.Selectemail();
            return dt;
        }





    }
}
