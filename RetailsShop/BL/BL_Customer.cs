﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;


namespace RetailsShop.BL
{
    class BL_Customer
    {



       
        private string firstname;
        private string middlename;
        private string lastname;
        private string phoneno;
        private string mobileno;
        private string email;
        private string country;
        private string state;
        private string city;
        private string address;
        private string customerid;
        static private int customeridstatic;
        private string type;

     

        public string FirstName
        {
            get { return firstname; }
            set { firstname = value; }
        }
        public string MiddleName
        {
            get { return middlename; }
            set { middlename = value; }
        }

        public string LastName
        {
            get { return lastname; }
            set { lastname = value; }
        }
        public string PhoneNo
        {
            get { return phoneno; }
            set { phoneno = value; }
        }


        public string Mobile
        {
            get { return mobileno; }
            set { mobileno = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }


        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        public string State
        {
            get { return state; }
            set { state = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public string CustomerId
        {
            get { return customerid; }
            set { customerid = value; }
        }
        public int CustomerIdStatic
        {
            get { return customeridstatic; }
            set { customeridstatic = value; }
        }
        public string Type
        {
            get { return type; }
            set { type = value; }
        }



        DL_Customer dl_customer = new DL_Customer();

        public void InsertCustomer(BL_Customer bl_customer)
        {
            dl_customer.InsertCustomer(bl_customer);
        }

        public DataTable SelectCustomer()
        {
            DataTable dt = dl_customer.SelectCustomer();
            return dt;
        }

        public void UpdateCustomer(BL_Customer bl_customer)
        {
            dl_customer.UpdateCustomer(bl_customer);
        }
        public void DeleteCustomer(BL_Customer bl_customer)
        {
            dl_customer.DeleteCustomer(bl_customer);
        }




    }
}
