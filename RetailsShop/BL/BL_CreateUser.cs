﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using RetailsShop.BL;
using RetailsShop.PL;
using RetailsShop.DL;

using System.Data;
using System.Data.SqlClient;



namespace RetailsShop.BL
{
    class BL_CreateUser
    {
        DL_CreateUser dl_createuser = new DL_CreateUser();
        BL_Field bl_field = new BL_Field();



        public DataTable SelectCreateUser()
        {
            DataTable dt= dl_createuser.SelectCreateUser();
            return dt;
        }


        public void InsertCreateUser(BL_Field bl_field)
        {
            dl_createuser.InsertCreateUser(bl_field);

 
        }


        public void UpdateCreateUser(BL_Field bl_field)
        {
            dl_createuser.UpdateCreateUser(bl_field);


        }



        public void DeleteCreateUser(BL_Field bl_field)
        {
            dl_createuser.DeleteCreateUser(bl_field);


        }




        public SqlDataReader SelectLogIn(BL_Field bl_field)
        {
            SqlDataReader dr = dl_createuser.SelectLogIn(bl_field);
            return dr;

        }


        //=============Staff==============


        public DataTable SelectStaff()
        {
            DataTable dt = dl_createuser.SelectStaff();
            return dt;
 
        }
       


    }
}
