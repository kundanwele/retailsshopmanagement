﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;


namespace RetailsShop.BL
{
    class BL_ProductName
    {
        DL_ProductName dl_productname = new DL_ProductName();



        private string supplierid;
        private string suppliername;
        private string manufacturename;
        private string productname;
        private string productnameid;
        static private int productnameidstatic;
        private string manufactureid;
        private string purchaseqty;
        private string purchaserate;
        private string salerate;
        private string reorderlevel;     
        private string Status;


        public string ManufactureName
        {
            get { return manufacturename; }
            set { manufacturename = value; }
        }

        public string ProductName
        {
            get { return productname; }
            set { productname = value; }
        }

        public string ProductNameId
        {
            get { return productnameid; }
            set { productnameid = value; }
        }
        public int ProductNameIdStatic
        {
            get { return productnameidstatic; }
            set { productnameidstatic = value; }
        }

        public string ManufactureId
        {
            get { return manufactureid; }
            set { manufactureid = value; }
        }
        public string PurchaseQty
        {
            get { return purchaseqty; }
            set { purchaseqty = value; }
        }

        public string PurchaseRate
        {
            get { return purchaserate; }
            set { purchaserate = value; }
        }
      
      

        public string SaleRate
        {
            get { return salerate; }
            set { salerate = value; }
        }    

        public string Reorderlevel
        {
            get { return reorderlevel; }
            set { reorderlevel = value; }
        }

        //public string Status
        //{
        //    get { return status; }
        //    set { status = value; }
        //}

        private string textsearch;

        public string TextSearch
        {
            get { return textsearch; }
            set { textsearch = value; }
        }


        public DataTable SelectProductName()
        {
            DataTable dt = dl_productname.SelectProductName();
            return dt;
        }

        public void InsertProductName(BL_ProductName bl_productname)
        {
            dl_productname.InsertProductName(bl_productname);
        }

        public void UpdateProductName(BL_ProductName bl_productname)
        {
            dl_productname.UpdateProductName(bl_productname);
        }
        public void DeleteProductName(BL_ProductName bl_productname)
        {
            dl_productname.DeleteProductName(bl_productname);

        }


        public DataTable SelectManufact()
        {
            DataTable dt = dl_productname.SelectManufact();
            return dt;
        }

        //public DataTable SelectProductName()
        //{
        //    DataTable dt = dl_productname.SelectProductName();
        //    return dt;
        //}
        public DataTable SelectProductNameBySearch(BL_ProductName bl_productname)
        {
            DataTable dt = dl_productname.SelectProductNameBySearch(bl_productname);
            return dt;
        }
        //public DataTable SelectProductNametoSale(BL_ProductName bl_productname)
        //{
        //    DataTable dt = dl_productname.SelectProductNametoSale(bl_productname);
        //    return dt;
        //}

    }
}
