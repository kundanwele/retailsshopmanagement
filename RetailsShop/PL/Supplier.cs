﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using RetailsShop.PL.Reports;

namespace RetailsShop.PL
{
    public partial class Supplier : Form
    {
       
        BL_Vendor bl_vendor = new BL_Vendor();

        public Supplier()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }


        private void btnadd_Click(object sender, EventArgs e)
        {
            
            SetField();
            bl_vendor.InsertVendor(bl_vendor);
            MessageBox.Show("Record inserted Successfully");
            BindVendor();
            Clear();

            btnadd.Enabled = true;
            btn_Update.Enabled = false;
            btndelete.Enabled = false;
        }
                
                    

        private void SetField()
        {
            bl_vendor.SupplierName = txtname.Text;
            bl_vendor.ContactPerson = txtcontactp.Text;
            bl_vendor.Country = txtcountry.Text;
            bl_vendor.State = txtstate.Text;
            bl_vendor.City = txtcity.Text;
            bl_vendor.Address = txtaddress.Text;
            bl_vendor.PhoneNo = txtphoneno.Text;
            bl_vendor.Mobile = txtmobileno.Text;
            bl_vendor.Email = txtemail.Text;
            bl_vendor.Note = txtnotes.Text;



        }


        private void Clear()
        {

            txtname.Text = "";
            txtcontactp.Text = "";
            txtcountry.Text = "";
            txtstate.Text = "";
            txtcity.Text = "";
            txtaddress.Text = "";
            txtphoneno.Text = "";
            txtmobileno.Text = "";
            txtemail.Text = "";
            txtnotes.Text = "";
            
        }


        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();

            btnadd.Enabled = true;
            btn_Update.Enabled = false;
            btndelete.Enabled = false;
           
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_vendor.DeleteVendor(bl_vendor);
                    txtname.Text = "";
                    MessageBox.Show("Record Delete Successfully");
                    BindVendor();
                    Clear();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }

            

            btnadd.Enabled = true;
            btn_Update.Enabled = false;
            btndelete.Enabled = false;

        }


        private void Supplier_Load(object sender, EventArgs e)
        {
            BindVendor();

            btn_Update.Enabled = false;
            btndelete.Enabled = false;


        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_vendor.SupplierId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            txtname.Text         = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            txtcontactp.Text     = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            txtcountry.Text      = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            txtstate.Text        = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            txtcity.Text         = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            txtaddress.Text      = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();
            txtphoneno.Text      = dataGridView1.SelectedRows[0].Cells[7].Value.ToString();
            txtmobileno.Text     = dataGridView1.SelectedRows[0].Cells[8].Value.ToString();
            txtemail.Text        = dataGridView1.SelectedRows[0].Cells[9].Value.ToString();
            txtnotes.Text        = dataGridView1.SelectedRows[0].Cells[10].Value.ToString();

            btnadd.Enabled = false;
            btn_Update.Enabled = true;
            btndelete.Enabled = true;
           
        }



        private void BindVendor()
        {
            DataTable dt = bl_vendor.SelectVendor();
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
                dataGridView1.Columns["SupplierId"].Visible = false;
                dataGridView1.Columns["Status"].Visible = false;
            }
            else
            {
                dataGridView1.DataSource = null;
            }
        }


        private void Supplier_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {

            SetField();
            bl_vendor.UpdateVendor(bl_vendor);
            MessageBox.Show("Record updated Successfully");
            BindVendor();
            Clear();


            btnadd.Enabled = true;
            btn_Update.Enabled = false;
            btndelete.Enabled = false;

        }



    }
}
