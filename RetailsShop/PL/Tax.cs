﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;


namespace RetailsShop.PL
{
    public partial class Tax : Form
    {
        BL_Tax bl_tax = new BL_Tax();

        public Tax()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void Tax_Load(object sender, EventArgs e)
        {
            BindGroup();

            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
        }

        private void SetField()
        {

            bl_tax.TaxDescription = txttaxdes.Text;
            bl_tax.TaxRate = txttaxrate.Text;
           
 
        }


        private void Clear()
        {
            txttaxdes.Text = "";
            txttaxrate.Text = "";
        }

        private void btnadd_Click(object sender, EventArgs e)
        {

            
               
                    if (txttaxdes.Text == "")
                    {
                        MessageBox .Show ("Please enter Tax Description");
                    }
                    else if (txttaxrate.Text == "")
                    { 
                    MessageBox .Show ("Please enter Tax Rate");
                    }
                    else 
                    {

                        SetField();
                        bl_tax.InsertTax(bl_tax);

                        Clear();
                        MessageBox.Show("Record inserted Successfully");
                        BindGroup();

                    }

                    btnadd.Enabled = true;
                    btnUpdate.Enabled = false;
                    btndelete.Enabled = false;
                           
            
            
        }


        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();

            btnadd.Enabled = true;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
           
        }


        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_tax.DeleteTax(bl_tax);

                    SetField();

                    Clear();
                    MessageBox.Show("Record Delete Successfully");
                    BindGroup();

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }


            btnadd.Enabled = true;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_tax.TaxId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();

            txttaxrate.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            txttaxdes.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();


            btnadd.Enabled = false;
            btnUpdate.Enabled = true;
            btndelete.Enabled = true;
          
           

        }


        private void BindGroup()
        {
            DataTable dt = bl_tax.SelectTax();
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
                dataGridView1.Columns["TaxId"].Visible = false;
                dataGridView1.Columns["Status"].Visible = false;
            }
            else
            {
                dataGridView1.DataSource = null;
            }
        }



        private void Tax_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }



        private void btnUpdate_Click(object sender, EventArgs e)
        {
            SetField();
            bl_tax.UpdateTax(bl_tax);

            Clear();
            MessageBox.Show("Record Updated Successfully");
            BindGroup();



            btnadd.Enabled = true;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;

        }

        private void txttaxrate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;

            }
        }
    }
}
