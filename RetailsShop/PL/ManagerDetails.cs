﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;


namespace RetailsShop.PL
{
    public partial class ManagerDetails : Form
    {
        DL_Manager dl_manger = new DL_Manager();
        BL_Manager bl_manager = new BL_Manager();

        public ManagerDetails()
        {
            InitializeComponent();
        }



        private void SetField()
        {
            bl_manager.Name = txtname.Text;
            bl_manager.MiddleName = txtmiddlename.Text;
            bl_manager.LastName = txtlastname.Text;
            bl_manager.PhoneNo = txtphone.Text;
            bl_manager.Mobile = txtmobileno.Text;
            bl_manager.Email = txtemail.Text;
            bl_manager.Country = txtcountry.Text;
            bl_manager.State = txtstate.Text;
            bl_manager.City = txtcity.Text;
            bl_manager.Address = txtaddress.Text;


        }


        private void ClearField()
        {
            txtname.Text = "";
            txtmiddlename.Text = "";
            txtlastname.Text = "";
            txtphone.Text = "";
            txtmobileno.Text = "";
            txtemail.Text = "";
            txtcountry.Text = "";
            txtstate.Text = "";
            txtcity.Text = "";
            txtaddress.Text = "";
            
        }


        private void BindManager()
        {
            DataTable dt = bl_manager.SelectManager();
            grdManager.DataSource = dt;
            grdManager.Columns["ManagerId"].Visible = false;
            grdManager.Columns["Status"].Visible = false;
        }


        private void ManagerDetails_Load(object sender, EventArgs e)
        {
            BindManager();

            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }

       
        private void btn_Save_Click(object sender, EventArgs e)
        {

            //f(email.IndexOf('@')==-1 || email.IndexOf('.')==-1)
            //{
            //    MessageBox.Show("Invalid Email Id");

            //}
           
            SetField();
            bl_manager.InsertManager(bl_manager);
            MessageBox.Show("Record Inserted successfully");
            ClearField();
            BindManager();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;

        }


        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            ClearField();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;


        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            SetField();
            bl_manager.UpdateManager(bl_manager);
            MessageBox.Show("Record Updated successfully");
            ClearField();
            BindManager();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;


        }



        private void btn_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_manager.DeleteManager(bl_manager);
                    ClearField();
                    MessageBox.Show("Record Deleted Successfully");
                    BindManager();
                    
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
           
        }



        private void grdManager_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                bl_manager.ManagerId = grdManager.SelectedRows[0].Cells[0].Value.ToString();
                txtname.Text         = grdManager.SelectedRows[0].Cells[1].Value.ToString();
                txtmiddlename.Text   = grdManager.SelectedRows[0].Cells[2].Value.ToString();
                txtlastname.Text     = grdManager.SelectedRows[0].Cells[3].Value.ToString();
                txtphone.Text        = grdManager.SelectedRows[0].Cells[4].Value.ToString();
                txtmobileno.Text     = grdManager.SelectedRows[0].Cells[5].Value.ToString();
                txtemail.Text        = grdManager.SelectedRows[0].Cells[6].Value.ToString();
                txtcountry.Text      = grdManager.SelectedRows[0].Cells[7].Value.ToString();
                txtstate.Text        = grdManager.SelectedRows[0].Cells[8].Value.ToString();
                txtcity.Text         = grdManager.SelectedRows[0].Cells[9].Value.ToString();
                txtaddress.Text      = grdManager.SelectedRows[0].Cells[10].Value.ToString();

            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }


            btn_Save.Enabled = false;
            btn_Update.Enabled = true;
            btn_Delete.Enabled = true;

        }

        private void txtphone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
               
            }

        }

        private void txtmobileno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
                
            }
        }


        private void ManagerDetails_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtcity_TextChanged(object sender, EventArgs e)
        {

        }


    }
}
