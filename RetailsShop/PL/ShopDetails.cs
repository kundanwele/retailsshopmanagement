﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using RetailsShop.PL;
using RetailsShop.BL;
using RetailsShop.DL;


using System.Data.SqlClient;



namespace RetailsShop.PL
{
    public partial class ShopDetails : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_OrganisationDetails bl_organisationdetails = new BL_OrganisationDetails();


        public ShopDetails()
        {
            InitializeComponent();
        }

        private void BindData()
        {
            
            BL_OrganisationDetails bl_organisationdetails = new BL_OrganisationDetails();
            DataTable dt = bl_organisationdetails.SelectOrganisationDetails();
            dgvShowOrganisationInfo.DataSource = dt;
           
            dgvShowOrganisationInfo.Columns["OrgId"].Visible = false;
        }

        private void Clear()
        {
            txtName.Text = "";
            txtAddress.Text = "";
            txtPhoneNo.Text = "";
            txtOfficeNo.Text = "";
           

        }


        //==============FormLoad======================



        private void OrganisationDetails_Load(object sender, EventArgs e)
        {
            //BindData();
        }


        //==============Save======================


        private void btn_Save_Click(object sender, EventArgs e)
        {

            if (txtName.Text == "")
            {
                MessageBox.Show("You can't leave Name empty");
            }

            else if (txtAddress.Text == "")
            {
                MessageBox.Show("You can't leave Address empty");
            }

            else if (txtOfficeNo.Text == "")
            {
                MessageBox.Show("You can't leave Office No empty");
            }

            else
            {
                bl_field.Name = txtName.Text;
                bl_field.Address = txtAddress.Text;
                bl_field.PhoneNo = txtPhoneNo.Text;
                bl_field.OfficeNo = txtOfficeNo.Text;

                bl_organisationdetails.InsertOrganisationDetails(bl_field);
                Clear();
                MessageBox.Show("Record Inserted Successfully");
                BindData();
            }

        }


        //==============Update======================


        private void btn_Update_Click(object sender, EventArgs e)
        {

            if (txtName.Text == "" || txtAddress.Text=="" || txtOfficeNo.Text=="")
            {
                MessageBox.Show("Please Select Specific Record");
            }
            else
            {
                bl_field.Name = txtName.Text;
                bl_field.Address = txtAddress.Text;
                bl_field.PhoneNo = txtPhoneNo.Text;
                bl_field.OfficeNo = txtOfficeNo.Text;

                bl_organisationdetails.UpdateOrganisationDetails(bl_field);
                Clear();
                MessageBox.Show("Record Updated Successfully");
                BindData();
            }


        }


        //==============Delete======================


        private void btn_Delete_Click(object sender, EventArgs e)
        {

            if (txtName.Text == "" || txtAddress.Text == "" || txtOfficeNo.Text == "")
            {
                MessageBox.Show("Please Select Specific Record");
            }
            else
            {

                try
                {

                    DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Record?", "Delete", MessageBoxButtons.YesNo);
                    if (msgg == DialogResult.Yes)
                    {

                        BL_OrganisationDetails bl_organisationdetails = new BL_OrganisationDetails();
                        bl_field.ShopId = dgvShowOrganisationInfo.SelectedRows[0].Cells[0].Value.ToString();
                        bl_organisationdetails.DeleteOrganisationDetails(bl_field);
                        MessageBox.Show("Record Delete Successfully");
                        BindData();
                        Clear();
                    }
                }
                catch
                {

                    MessageBox.Show("error occured!Please contact Administration?");
                }
            }


        }



        //==============Cancel======================



        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Clear();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;

        }



        //==============GridView======================



        private void dgvShowOrganisationInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvShowOrganisationInfo.Rows.Count > 0)
            {

                bl_field.ShopId = dgvShowOrganisationInfo.SelectedRows[0].Cells[0].Value.ToString();
                
                txtName.Text = dgvShowOrganisationInfo.SelectedRows[0].Cells[1].Value.ToString(); ;
                txtAddress.Text = dgvShowOrganisationInfo.SelectedRows[0].Cells[2].Value.ToString(); ;
                txtPhoneNo.Text = dgvShowOrganisationInfo.SelectedRows[0].Cells[3].Value.ToString(); ;
                txtOfficeNo.Text = dgvShowOrganisationInfo.SelectedRows[0].Cells[4].Value.ToString(); ;
           
            }

            btn_Save.Enabled = false;
            btn_Update.Enabled = true;
            btn_Delete.Enabled = true;


        }

        private void OrganisationDetails_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtPhoneNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            //common c = new common();
            //c.NumberOnly(e);
        }

        private void txtOfficeNo_KeyPress(object sender, KeyPressEventArgs e)
        {

            //common c = new common();
            //c.NumberOnly(e);
        }


        
    }
}
