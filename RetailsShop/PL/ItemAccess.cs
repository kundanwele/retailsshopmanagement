﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;


namespace RetailsShop.PL
{
    public partial class ItemAccess : Form
    {

        BL_TreeView bl_treeview = new BL_TreeView();


        public ItemAccess()
        {
            InitializeComponent();
        }

        private void ItemAccess_Load(object sender, EventArgs e)
        {
            BindCatagory();

            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
           
                    if (txtitem.Text == "")
                    {
                        MessageBox.Show("Please enter Name?");
                    }
                    else
                    {
                        Insert();
                    }



              
                    

          
 

        }


        private void Insert()
        {
   
                SetField();
                bl_treeview.InsertTreeViewItem(bl_treeview);

                MessageBox.Show("Record inserted Successfully");
                string catid = ddlcatagory.SelectedValue.ToString();

                BindGrid(catid);
                txtitem.Focus();
                clear();

                btnadd.Enabled = true;
                btnUpdate.Enabled = false;
                btndelete.Enabled = false;
       
       
        }

        private void SetField()
        {
            bl_treeview.MenuId = Convert.ToInt32(ddlcatagory.SelectedValue.ToString());
            bl_treeview.ItemName = txtitem.Text;
        }



               private void BindCatagory()
        {
            DataTable dt = bl_treeview.SelectTreeViewMenu(bl_treeview);
            ddlcatagory.DataSource = dt;
            ddlcatagory.ValueMember = "MenuId";
            ddlcatagory.DisplayMember = "MenuName";
        }


        private void BindGrid(string catid)
        {
            DataTable dt = bl_treeview.SelectTreeViewItem(catid);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["ItemId"].Visible = false;
            dataGridView1.Columns["MenuId"].Visible = false;

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_treeview.ItemId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            txtitem.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();

            btnadd.Enabled = false;
            btnUpdate.Enabled = true;
            btndelete.Enabled = true;
           
        }

        private void clear()
        {
            txtitem.Text = "";

            btnadd.Enabled = true;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
          
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_treeview.ItemId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                    bl_treeview.DeleteTreeViewItem(bl_treeview);
                    txtitem.Text = "";
                    MessageBox.Show("Record Delete Successfully");
                    string catid = ddlcatagory.SelectedValue.ToString();

                    BindGrid(catid);
                    clear();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }

            btnadd.Enabled = true;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
        }


        private void ddlcatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlcatagory.Text != "System.Data.DataRowView")
            {
                if (ddlcatagory.ValueMember == "MenuId")
                {
                    string catid = ddlcatagory.SelectedValue.ToString();
                    BindGrid(catid);
                }
            }
        }



        private void btnUpdate_Click(object sender, EventArgs e)
        {
            SetField();
            bl_treeview.UpdateTreeViewItem(bl_treeview);
            clear();
            MessageBox.Show("Record Updated Successfully");
            string catid = ddlcatagory.SelectedValue.ToString();

            BindGrid(catid);
            clear();

            btnadd.Enabled = true;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void ddlcatagory_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (ddlcatagory.Text != "System.Data.DataRowView")
            {
                if (ddlcatagory.ValueMember == "MenuId")
                {
                    string catid = ddlcatagory.SelectedValue.ToString();
                    BindGrid(catid);
                }
            }

        }

        




    }
}
