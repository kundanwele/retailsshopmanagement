﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;



namespace RetailsShop.PL
{

    public partial class Worker : Form
    {
        BL_Worker bl_worker = new BL_Worker();
        BL_Field bl_field = new BL_Field();

        public Worker()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
                   
                        SetField();
                        bl_worker.InsertWorker(bl_field);
                        MessageBox.Show("Records Added successfully");
                        ClearField();
                        BindWorker();

                        button1.Enabled = true;
                        btnUpdate.Enabled = false;
                        btndelete.Enabled = false;


        }





        private void SetField()
        {
            bl_field.Name = txtname.Text;
            bl_field.MiddleName = txtmiddlename.Text;
            bl_field.LastName = txtlastname.Text;
            bl_field.PhoneNo = txtphone.Text;
            bl_field.Mobile = txtmobileno.Text;
            bl_field.Email = txtemail.Text;
            bl_field.Country = txtcountry.Text;
            bl_field.State = txtstate.Text;
            bl_field.City = txtcity.Text;
            bl_field.Address = txtaddress.Text;
            bl_field.Role = ddlsecurty.Text;
            bl_field.Username = txtusername.Text;


        }
        private void ClearField()
        {
                txtname.Text="";
                txtmiddlename.Text="";
                txtlastname.Text="";
                txtphone.Text  ="";
                txtmobileno.Text="";
                txtemail.Text="";
                txtcountry.Text="";
                txtstate.Text="";
                txtcity.Text="";
                txtaddress.Text="";
                ddlsecurty.Text = "";
                txtusername.Text = "";
                txtpassword.Text = "";
        }
        private void Worker_Load(object sender, EventArgs e)
        {
            BindWorker();

            btnUpdate.Enabled = false;
            btndelete.Enabled = false;

        }

        private void grdworker_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                bl_field.WorkerId  = grdworker.SelectedRows[0].Cells[0].Value.ToString();
                txtname.Text       = grdworker.SelectedRows[0].Cells[1].Value.ToString();
                txtmiddlename.Text = grdworker.SelectedRows[0].Cells[2].Value.ToString();
                txtlastname.Text   = grdworker.SelectedRows[0].Cells[3].Value.ToString();
                txtphone.Text      = grdworker.SelectedRows[0].Cells[4].Value.ToString();
                txtmobileno.Text   = grdworker.SelectedRows[0].Cells[5].Value.ToString();
                txtemail.Text      = grdworker.SelectedRows[0].Cells[6].Value.ToString();
                txtcountry.Text    = grdworker.SelectedRows[0].Cells[7].Value.ToString();
                txtstate.Text      = grdworker.SelectedRows[0].Cells[8].Value.ToString();
                txtcity.Text       = grdworker.SelectedRows[0].Cells[9].Value.ToString();
                txtaddress.Text    = grdworker.SelectedRows[0].Cells[10].Value.ToString();
                ddlsecurty.Text    = grdworker.SelectedRows[0].Cells[11].Value.ToString();
                txtusername.Text   = grdworker.SelectedRows[0].Cells[12].Value.ToString();
                txtpassword.Text   = grdworker.SelectedRows[0].Cells[13].Value.ToString();
                
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }

            button1.Enabled = false;
            btnUpdate.Enabled = true;
            btndelete.Enabled = true;
            


        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_worker.DeleteWorker(bl_field);
                    ClearField();
                    MessageBox.Show("Record Delete Successfully");
                    BindWorker();
                    
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }

            button1.Enabled = true;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;


        }



        private void BindWorker()
        {
            DataTable dt = bl_worker.SelectWorker();
            grdworker.DataSource = dt;
            grdworker.Columns["WorkerId"].Visible = false;
            grdworker.Columns["Password"].Visible = false;
        }



        private void btncancel_Click(object sender, EventArgs e)
        {
            ClearField();

           button1.Enabled = true;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
          
        }



        private void txtphone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Please enter number only");
            }
        }

        private void Worker_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtmobileno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Please enter number only");
            }
        }

       

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            SetField();
            bl_worker.UpdateWorker(bl_field);
            MessageBox.Show("Records Updated successfully");
            ClearField();
            BindWorker();

            button1.Enabled = true;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
           

        }

        private void ddlsecurty_SelectedIndexChanged(object sender, EventArgs e)
        {

        }






























    }
}
