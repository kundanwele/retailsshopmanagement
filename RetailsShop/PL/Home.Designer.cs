﻿namespace RetailsShop.PL
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saleStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shopDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageAccessLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.followUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.masterSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workerDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smsSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printerSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAccessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemAccessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryBoyDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manufacturerDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.damageEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saleReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addItemsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notePadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendSmsEmailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.daywiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.financeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expensesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolToolStripMenuItem,
            this.masterSetupToolStripMenuItem,
            this.transactionToolStripMenuItem,
            this.inventoryToolStripMenuItem,
            this.financeToolStripMenuItem,
            this.utilityToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.logOutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 29);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saleStatusToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(49, 25);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // saleStatusToolStripMenuItem
            // 
            this.saleStatusToolStripMenuItem.Name = "saleStatusToolStripMenuItem";
            this.saleStatusToolStripMenuItem.Size = new System.Drawing.Size(182, 32);
            this.saleStatusToolStripMenuItem.Text = "SaleStatus";
            this.saleStatusToolStripMenuItem.Click += new System.EventHandler(this.saleStatusToolStripMenuItem_Click);
            // 
            // toolToolStripMenuItem
            // 
            this.toolToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shopDetailsToolStripMenuItem,
            this.manageAccessLevelToolStripMenuItem,
            this.followUpToolStripMenuItem,
            this.createUserToolStripMenuItem});
            this.toolToolStripMenuItem.Name = "toolToolStripMenuItem";
            this.toolToolStripMenuItem.Size = new System.Drawing.Size(66, 32);
            this.toolToolStripMenuItem.Text = "Tool";
            // 
            // shopDetailsToolStripMenuItem
            // 
            this.shopDetailsToolStripMenuItem.Name = "shopDetailsToolStripMenuItem";
            this.shopDetailsToolStripMenuItem.Size = new System.Drawing.Size(272, 32);
            this.shopDetailsToolStripMenuItem.Text = "ShopDetails";
            this.shopDetailsToolStripMenuItem.Click += new System.EventHandler(this.shopDetailsToolStripMenuItem_Click);
            // 
            // manageAccessLevelToolStripMenuItem
            // 
            this.manageAccessLevelToolStripMenuItem.Name = "manageAccessLevelToolStripMenuItem";
            this.manageAccessLevelToolStripMenuItem.Size = new System.Drawing.Size(272, 32);
            this.manageAccessLevelToolStripMenuItem.Text = "ManageAccessLevel";
            this.manageAccessLevelToolStripMenuItem.Click += new System.EventHandler(this.manageAccessLevelToolStripMenuItem_Click);
            // 
            // followUpToolStripMenuItem
            // 
            this.followUpToolStripMenuItem.Name = "followUpToolStripMenuItem";
            this.followUpToolStripMenuItem.Size = new System.Drawing.Size(272, 32);
            this.followUpToolStripMenuItem.Text = "FollowUp";
            // 
            // createUserToolStripMenuItem
            // 
            this.createUserToolStripMenuItem.Name = "createUserToolStripMenuItem";
            this.createUserToolStripMenuItem.Size = new System.Drawing.Size(272, 32);
            this.createUserToolStripMenuItem.Text = "CreateUser";
            this.createUserToolStripMenuItem.Click += new System.EventHandler(this.createUserToolStripMenuItem_Click);
            // 
            // masterSetupToolStripMenuItem
            // 
            this.masterSetupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.supplierDetailsToolStripMenuItem,
            this.workerDetailsToolStripMenuItem,
            this.smsSettingToolStripMenuItem,
            this.emailSettingToolStripMenuItem,
            this.printerSettingToolStripMenuItem,
            this.menuAccessToolStripMenuItem,
            this.itemAccessToolStripMenuItem,
            this.customerDetailsToolStripMenuItem,
            this.deliveryBoyDetailsToolStripMenuItem,
            this.manaToolStripMenuItem,
            this.manufacturerDetailsToolStripMenuItem,
            this.taxToolStripMenuItem,
            this.paymentModeToolStripMenuItem});
            this.masterSetupToolStripMenuItem.Name = "masterSetupToolStripMenuItem";
            this.masterSetupToolStripMenuItem.Size = new System.Drawing.Size(144, 32);
            this.masterSetupToolStripMenuItem.Text = "MasterSetup";
            // 
            // supplierDetailsToolStripMenuItem
            // 
            this.supplierDetailsToolStripMenuItem.Name = "supplierDetailsToolStripMenuItem";
            this.supplierDetailsToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.supplierDetailsToolStripMenuItem.Text = "SupplierDetails";
            this.supplierDetailsToolStripMenuItem.Click += new System.EventHandler(this.supplierDetailsToolStripMenuItem_Click);
            // 
            // workerDetailsToolStripMenuItem
            // 
            this.workerDetailsToolStripMenuItem.Name = "workerDetailsToolStripMenuItem";
            this.workerDetailsToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.workerDetailsToolStripMenuItem.Text = "WorkerDetails";
            this.workerDetailsToolStripMenuItem.Click += new System.EventHandler(this.workerDetailsToolStripMenuItem_Click);
            // 
            // smsSettingToolStripMenuItem
            // 
            this.smsSettingToolStripMenuItem.Name = "smsSettingToolStripMenuItem";
            this.smsSettingToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.smsSettingToolStripMenuItem.Text = "SmsSetting";
            this.smsSettingToolStripMenuItem.Click += new System.EventHandler(this.smsSettingToolStripMenuItem_Click);
            // 
            // emailSettingToolStripMenuItem
            // 
            this.emailSettingToolStripMenuItem.Name = "emailSettingToolStripMenuItem";
            this.emailSettingToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.emailSettingToolStripMenuItem.Text = "EmailSetting";
            this.emailSettingToolStripMenuItem.Click += new System.EventHandler(this.emailSettingToolStripMenuItem_Click);
            // 
            // printerSettingToolStripMenuItem
            // 
            this.printerSettingToolStripMenuItem.Name = "printerSettingToolStripMenuItem";
            this.printerSettingToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.printerSettingToolStripMenuItem.Text = "PrinterSetting";
            this.printerSettingToolStripMenuItem.Click += new System.EventHandler(this.printerSettingToolStripMenuItem_Click);
            // 
            // menuAccessToolStripMenuItem
            // 
            this.menuAccessToolStripMenuItem.Name = "menuAccessToolStripMenuItem";
            this.menuAccessToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.menuAccessToolStripMenuItem.Text = "MenuAccess";
            this.menuAccessToolStripMenuItem.Click += new System.EventHandler(this.menuAccessToolStripMenuItem_Click);
            // 
            // itemAccessToolStripMenuItem
            // 
            this.itemAccessToolStripMenuItem.Name = "itemAccessToolStripMenuItem";
            this.itemAccessToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.itemAccessToolStripMenuItem.Text = "ItemAccess";
            this.itemAccessToolStripMenuItem.Click += new System.EventHandler(this.itemAccessToolStripMenuItem_Click);
            // 
            // customerDetailsToolStripMenuItem
            // 
            this.customerDetailsToolStripMenuItem.Name = "customerDetailsToolStripMenuItem";
            this.customerDetailsToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.customerDetailsToolStripMenuItem.Text = "CustomerDetails";
            this.customerDetailsToolStripMenuItem.Click += new System.EventHandler(this.customerDetailsToolStripMenuItem_Click);
            // 
            // deliveryBoyDetailsToolStripMenuItem
            // 
            this.deliveryBoyDetailsToolStripMenuItem.Name = "deliveryBoyDetailsToolStripMenuItem";
            this.deliveryBoyDetailsToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.deliveryBoyDetailsToolStripMenuItem.Text = "DeliveryBoyDetails";
            this.deliveryBoyDetailsToolStripMenuItem.Click += new System.EventHandler(this.deliveryBoyDetailsToolStripMenuItem_Click);
            // 
            // manaToolStripMenuItem
            // 
            this.manaToolStripMenuItem.Name = "manaToolStripMenuItem";
            this.manaToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.manaToolStripMenuItem.Text = "ManagerDetails";
            this.manaToolStripMenuItem.Click += new System.EventHandler(this.manaToolStripMenuItem_Click);
            // 
            // manufacturerDetailsToolStripMenuItem
            // 
            this.manufacturerDetailsToolStripMenuItem.Name = "manufacturerDetailsToolStripMenuItem";
            this.manufacturerDetailsToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.manufacturerDetailsToolStripMenuItem.Text = "ManufacturerDetails";
            this.manufacturerDetailsToolStripMenuItem.Click += new System.EventHandler(this.manufacturerDetailsToolStripMenuItem_Click);
            // 
            // taxToolStripMenuItem
            // 
            this.taxToolStripMenuItem.Name = "taxToolStripMenuItem";
            this.taxToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.taxToolStripMenuItem.Text = "Tax";
            this.taxToolStripMenuItem.Click += new System.EventHandler(this.taxToolStripMenuItem_Click);
            // 
            // paymentModeToolStripMenuItem
            // 
            this.paymentModeToolStripMenuItem.Name = "paymentModeToolStripMenuItem";
            this.paymentModeToolStripMenuItem.Size = new System.Drawing.Size(280, 32);
            this.paymentModeToolStripMenuItem.Text = "PaymentMode";
            this.paymentModeToolStripMenuItem.Click += new System.EventHandler(this.paymentModeToolStripMenuItem_Click);
            // 
            // transactionToolStripMenuItem
            // 
            this.transactionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.invoiceToolStripMenuItem,
            this.purchaseToolStripMenuItem,
            this.purchaseReturnToolStripMenuItem,
            this.saleReturnToolStripMenuItem,
            this.damageEntryToolStripMenuItem});
            this.transactionToolStripMenuItem.Name = "transactionToolStripMenuItem";
            this.transactionToolStripMenuItem.Size = new System.Drawing.Size(111, 25);
            this.transactionToolStripMenuItem.Text = "Transaction";
            // 
            // damageEntryToolStripMenuItem
            // 
            this.damageEntryToolStripMenuItem.Name = "damageEntryToolStripMenuItem";
            this.damageEntryToolStripMenuItem.Size = new System.Drawing.Size(233, 32);
            this.damageEntryToolStripMenuItem.Text = "DamageEntry";
            // 
            // purchaseToolStripMenuItem
            // 
            this.purchaseToolStripMenuItem.Name = "purchaseToolStripMenuItem";
            this.purchaseToolStripMenuItem.Size = new System.Drawing.Size(233, 32);
            this.purchaseToolStripMenuItem.Text = "Purchase";
            this.purchaseToolStripMenuItem.Click += new System.EventHandler(this.purchaseToolStripMenuItem_Click);
            // 
            // purchaseReturnToolStripMenuItem
            // 
            this.purchaseReturnToolStripMenuItem.Name = "purchaseReturnToolStripMenuItem";
            this.purchaseReturnToolStripMenuItem.Size = new System.Drawing.Size(233, 32);
            this.purchaseReturnToolStripMenuItem.Text = "PurchaseReturn";
            // 
            // saleReturnToolStripMenuItem
            // 
            this.saleReturnToolStripMenuItem.Name = "saleReturnToolStripMenuItem";
            this.saleReturnToolStripMenuItem.Size = new System.Drawing.Size(233, 32);
            this.saleReturnToolStripMenuItem.Text = "SaleReturn";
            // 
            // invoiceToolStripMenuItem
            // 
            this.invoiceToolStripMenuItem.Name = "invoiceToolStripMenuItem";
            this.invoiceToolStripMenuItem.Size = new System.Drawing.Size(233, 32);
            this.invoiceToolStripMenuItem.Text = "Sale";
            this.invoiceToolStripMenuItem.Click += new System.EventHandler(this.invoiceToolStripMenuItem_Click);
            // 
            // inventoryToolStripMenuItem
            // 
            this.inventoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewStockToolStripMenuItem,
            this.addItemsToolStripMenuItem});
            this.inventoryToolStripMenuItem.Name = "inventoryToolStripMenuItem";
            this.inventoryToolStripMenuItem.Size = new System.Drawing.Size(115, 32);
            this.inventoryToolStripMenuItem.Text = "Inventory";
            // 
            // viewStockToolStripMenuItem
            // 
            this.viewStockToolStripMenuItem.Name = "viewStockToolStripMenuItem";
            this.viewStockToolStripMenuItem.Size = new System.Drawing.Size(252, 32);
            this.viewStockToolStripMenuItem.Text = "ViewStock";
            this.viewStockToolStripMenuItem.Click += new System.EventHandler(this.viewStockToolStripMenuItem_Click);
            // 
            // addItemsToolStripMenuItem
            // 
            this.addItemsToolStripMenuItem.Name = "addItemsToolStripMenuItem";
            this.addItemsToolStripMenuItem.Size = new System.Drawing.Size(252, 32);
            this.addItemsToolStripMenuItem.Text = "AddProductName";
            this.addItemsToolStripMenuItem.Click += new System.EventHandler(this.addItemsToolStripMenuItem_Click);
            // 
            // utilityToolStripMenuItem
            // 
            this.utilityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculatorToolStripMenuItem,
            this.notePadToolStripMenuItem,
            this.sendSmsEmailToolStripMenuItem,
            this.loginHistoryToolStripMenuItem});
            this.utilityToolStripMenuItem.Name = "utilityToolStripMenuItem";
            this.utilityToolStripMenuItem.Size = new System.Drawing.Size(83, 32);
            this.utilityToolStripMenuItem.Text = "Utility";
            // 
            // calculatorToolStripMenuItem
            // 
            this.calculatorToolStripMenuItem.Name = "calculatorToolStripMenuItem";
            this.calculatorToolStripMenuItem.Size = new System.Drawing.Size(229, 32);
            this.calculatorToolStripMenuItem.Text = "Calculator";
            this.calculatorToolStripMenuItem.Click += new System.EventHandler(this.calculatorToolStripMenuItem_Click);
            // 
            // notePadToolStripMenuItem
            // 
            this.notePadToolStripMenuItem.Name = "notePadToolStripMenuItem";
            this.notePadToolStripMenuItem.Size = new System.Drawing.Size(229, 32);
            this.notePadToolStripMenuItem.Text = "NotePad";
            this.notePadToolStripMenuItem.Click += new System.EventHandler(this.notePadToolStripMenuItem_Click);
            // 
            // sendSmsEmailToolStripMenuItem
            // 
            this.sendSmsEmailToolStripMenuItem.Name = "sendSmsEmailToolStripMenuItem";
            this.sendSmsEmailToolStripMenuItem.Size = new System.Drawing.Size(229, 32);
            this.sendSmsEmailToolStripMenuItem.Text = "SendSms/Email";
            this.sendSmsEmailToolStripMenuItem.Click += new System.EventHandler(this.sendSmsEmailToolStripMenuItem_Click);
            // 
            // loginHistoryToolStripMenuItem
            // 
            this.loginHistoryToolStripMenuItem.Name = "loginHistoryToolStripMenuItem";
            this.loginHistoryToolStripMenuItem.Size = new System.Drawing.Size(229, 32);
            this.loginHistoryToolStripMenuItem.Text = "LoginHistory";
            this.loginHistoryToolStripMenuItem.Click += new System.EventHandler(this.loginHistoryToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseToolStripMenuItem1});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(97, 32);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // purchaseToolStripMenuItem1
            // 
            this.purchaseToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.daywiseToolStripMenuItem});
            this.purchaseToolStripMenuItem1.Name = "purchaseToolStripMenuItem1";
            this.purchaseToolStripMenuItem1.Size = new System.Drawing.Size(169, 32);
            this.purchaseToolStripMenuItem1.Text = "Purchase";
            // 
            // daywiseToolStripMenuItem
            // 
            this.daywiseToolStripMenuItem.Name = "daywiseToolStripMenuItem";
            this.daywiseToolStripMenuItem.Size = new System.Drawing.Size(169, 32);
            this.daywiseToolStripMenuItem.Text = "Day wise";
            this.daywiseToolStripMenuItem.Click += new System.EventHandler(this.daywiseToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(68, 32);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(60, 32);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(93, 32);
            this.logOutToolStripMenuItem.Text = "LogOut";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // financeToolStripMenuItem
            // 
            this.financeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.expensesToolStripMenuItem});
            this.financeToolStripMenuItem.Name = "financeToolStripMenuItem";
            this.financeToolStripMenuItem.Size = new System.Drawing.Size(96, 32);
            this.financeToolStripMenuItem.Text = "Finance";
            // 
            // expensesToolStripMenuItem
            // 
            this.expensesToolStripMenuItem.Name = "expensesToolStripMenuItem";
            this.expensesToolStripMenuItem.Size = new System.Drawing.Size(170, 32);
            this.expensesToolStripMenuItem.Text = "Expenses";
            this.expensesToolStripMenuItem.Click += new System.EventHandler(this.expensesToolStripMenuItem_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Ivory;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Home";
            this.Text = "K-Palms Help Line 9096234202,9595163594/  www.kpalms.net";
            this.Load += new System.EventHandler(this.Home_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem masterSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saleStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shopDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageAccessLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem followUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem workerDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smsSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printerSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuAccessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemAccessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deliveryBoyDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manufacturerDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem damageEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseReturnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saleReturnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewStockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addItemsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notePadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendSmsEmailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem financeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expensesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invoiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem daywiseToolStripMenuItem;
    }
}