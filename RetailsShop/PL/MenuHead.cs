﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using RetailsShop.BL;
namespace RetailsShop
{
    public partial class MenuHead : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_TreeView bl_treeview = new BL_TreeView();
  

        public MenuHead()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void MenuHead_Load(object sender, EventArgs e)
        {
            BindMenu();

           
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
           
                    if (txtname.Text == "")
                    {
                        MessageBox.Show("Please enter Name?");
                    }
                    else
                    {

                        SetField();
                        bl_treeview.InsertTreeViewMenu(bl_treeview);
                        Clear();
                        MessageBox.Show("Record inserted Successfully");
                        BindMenu();
                        txtname.Focus();
                    }
               
        }

        private void SetField()
        {
            bl_treeview.MenuName = txtname.Text;

        }

        private void Clear()
        {
            txtname.Text = "";

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_treeview.MenuId = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            txtname.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();

            btnadd.Enabled = false;
            btnUpdate.Enabled = true;
            btndelete.Enabled = true;
            
        }

        private void BindMenu()
        {
            DataTable dt = bl_treeview.SelectTreeViewMenu(bl_treeview);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["MenuId"].Visible = false;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();

            btnadd.Enabled = true;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
            
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_treeview.DeleteTreeViewMenu(bl_treeview);
                    MessageBox.Show("Record Delete Successfully");
                    BindMenu();
                    Clear();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }


            btnadd.Enabled = true;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
        }

        private void MenuHead_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            
            SetField();
            bl_treeview.UpdateTreeViewMenu(bl_treeview);
            Clear();
            MessageBox.Show("Record Updated Successfully");
            BindMenu();

            btnadd.Enabled = true;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
        
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

      






    }
}
