﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



using System.Data.SqlClient;
using RetailsShop.BL;


namespace RetailsShop.PL
{
    public partial class PrinterSetup : Form
    {

        BL_PrinterSetup bl_PrinterSetup = new BL_PrinterSetup();
        


        public PrinterSetup()
        {
            InitializeComponent();
        }



        private void PrinterSetup_Load(object sender, EventArgs e)
        {
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                ddlbill.Items.Add(printer);
                ddlkot.Items.Add(printer);
            }

            BindPrinter();
        }




        private void SetField()
        {
            bl_PrinterSetup.BillPrinter = ddlbill.Text;
            bl_PrinterSetup.KotPrinter = ddlkot.Text;
        }




        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btnadd_Click(object sender, EventArgs e)
        {
            
                  if (ddlbill.Text == "")
                    {
                        MessageBox.Show("Please select Bill Printer?");
                    }
                    else if (ddlkot.Text == "")
                    {
                        MessageBox.Show("Please select KOT Printer?");
                    }
                    else
                    {

                        SetField();
                        bl_PrinterSetup.InsertPrinter(bl_PrinterSetup);

                        MessageBox.Show("Printer select Successfully");
                        BindPrinter();

                    }               
               

                
            
            
        }


        private void BindPrinter()
        {
            DataTable dt = bl_PrinterSetup.SelectPrinter();
            if (dt.Rows.Count > 0)
            {
                ddlbill.Text = dt.Rows[0][1].ToString();
                ddlkot.Text = dt.Rows[0][2].ToString();
               
            }
            

        }

        private void ddlbill_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
