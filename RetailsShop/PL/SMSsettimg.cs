﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using RetailsShop.PL;
using RetailsShop.BL;
using RetailsShop.DL;

using System.Data.SqlClient;



namespace RetailsShop.PL
{
    public partial class SMSsettimg : Form
    {
        BL_SMSsetting bl_smssetting = new BL_SMSsetting();
        //BL_Field bl_field = new BL_Field();


        public SMSsettimg()
        {
            InitializeComponent();
        }


        private void BindData()
        {

            DataTable dt = bl_smssetting.SelectSmsSetting();
            dgvShowSmsSettingInfo.DataSource = dt;
           

            dgvShowSmsSettingInfo.Columns["SmsId"].Visible = false;
        }

        private void Clear()
        {
            txtUrl.Text = "";
            txtUserId.Text = "";
            txtPassword.Text = "";
        }


        //==========Form Load============



        private void SMSsettimg_Load(object sender, EventArgs e)
        {
           BindData();

           btn_Update.Enabled = false;
           btn_Delete.Enabled = false;
        }


        //==========SAVE============


        private void btn_Save_Click(object sender, EventArgs e)
        {

            if (txtUrl.Text == "")
            {
                MessageBox.Show("You Can't leave URL empty");
            }
            else if (txtUserId.Text == "")
            {
                MessageBox.Show("You Can't leave UserId empty");
            }
            else if(txtPassword.Text=="")
            {
                MessageBox.Show("You Can't leave Password empty");
            }

            else
            {

                bl_smssetting.Url = txtUrl.Text;
                bl_smssetting.UserId = txtUserId.Text;
                bl_smssetting.Password = txtPassword.Text;
                bl_smssetting.InsertSmsSetting(bl_smssetting);
                Clear();
                MessageBox.Show("Record Inserted Successfully");
                BindData();

                btn_Update.Enabled = false;
                btn_Delete.Enabled = false;
            }


        }


        //==========Update============


        private void btn_Update_Click(object sender, EventArgs e)
        {
            if (txtUrl.Text == "" || txtUserId.Text=="" || txtPassword.Text=="")
            {
                MessageBox.Show("Please Select Specific Record");
            }

            else
            {

                bl_smssetting.Url = txtUrl.Text;
                bl_smssetting.UserId = txtUserId.Text;
                bl_smssetting.Password = txtPassword.Text;
                bl_smssetting.UpdateSmsSetting(bl_smssetting);
                Clear();
                MessageBox.Show("Record Update Successfully");
                BindData();
            }

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;

        }


        //==========DELETE============


        private void btn_Delete_Click(object sender, EventArgs e)
        {
            if (txtUrl.Text == "" || txtUserId.Text == "" || txtPassword.Text == "")
            {
                MessageBox.Show("Please Select Specific Record");
            }
            else
            {
                try
                {

                    DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Record?", "Delete", MessageBoxButtons.YesNo);
                    if (msgg == DialogResult.Yes)
                    {

                        bl_smssetting.SmsId = dgvShowSmsSettingInfo.SelectedRows[0].Cells[0].Value.ToString();
                        bl_smssetting.DeleteSmsSetting(bl_smssetting);
                        MessageBox.Show("Record Delete Successfully");
                        BindData();
                        Clear();
                    }
                }
                catch
                {

                    MessageBox.Show("error occured!Please contact Administration?");
                }

                btn_Save.Enabled = true;
                btn_Update.Enabled = false;
                btn_Delete.Enabled = false;
            }

        }



        //==========CANCEL============



        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Clear();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;

        }



        //==========GRID VIEW============



        private void dgvShowSmsSettingInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvShowSmsSettingInfo.Rows.Count > 0)
            {

                bl_smssetting.SmsId = dgvShowSmsSettingInfo.SelectedRows[0].Cells[0].Value.ToString();
                txtUrl.Text = dgvShowSmsSettingInfo.SelectedRows[0].Cells[1].Value.ToString(); ;
                txtUserId.Text = dgvShowSmsSettingInfo.SelectedRows[0].Cells[2].Value.ToString(); ;
                txtPassword.Text = dgvShowSmsSettingInfo.SelectedRows[0].Cells[3].Value.ToString(); ;
            }

            btn_Save.Enabled = false;
            btn_Update.Enabled = true;
            btn_Delete.Enabled = true;


        }

        private void SMSsettimg_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
