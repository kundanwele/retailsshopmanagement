﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using RetailsShop.BL;
using RetailsShop.PL;
using RetailsShop.DL;

using System.Data.SqlClient;


namespace RetailsShop.PL
{
    public partial class OtherExpences : Form
    {
        BL_OtherExpences bl_otherexpences = new BL_OtherExpences();
        BL_Field bl_field = new BL_Field();

        public OtherExpences()
        {
            InitializeComponent();
        }




      



        //=======Bind Data=========
        


        private void BindData()
        {
            DataTable dt = bl_otherexpences.SelectOtherExpences();
            dgvShowOtherExpenseInfo.DataSource = dt;

            dgvShowOtherExpenseInfo.Columns["ExpensesId"].Visible = false;
        }



        //==================Clear=============



        private void Clear()
        {
            dateTimePicker1.Text = "";
           
            txtExpenseName.Text  = "";
            txtAmount.Text       = "";
            txtRemark.Text       = "";
        }




        //==================Set Field================




        private void SetField()
        {
            bl_field.Date        = dateTimePicker1.Text;
         
            bl_field.ExpenseName = txtExpenseName.Text;
            bl_field.Amount      = txtAmount.Text;
            bl_field.Remark      = txtRemark.Text;
        }




        //=======================Save=====================




        private void btn_Save_Click(object sender, EventArgs e)
        {
            if(txtExpenseName.Text=="")
            {
                MessageBox.Show("You Can't leave Expense Name empty");
            }
            else if(txtAmount.Text=="")
            {
                MessageBox.Show("You Can't leave Amount empty");
            }
            
            else
            {

                SetField();
                bl_otherexpences.InsertOtherExpences(bl_field);

                Clear();
                MessageBox.Show("Record Inserted Successfully");
                BindData();
            }

        }



        //=======================Update=====================




        private void btn_Update_Click(object sender, EventArgs e)
        {
            if(txtExpenseName.Text=="" || txtAmount.Text=="")
            {
                MessageBox.Show("Please Select Specific Record");
            }
            else
            {

                SetField();
                bl_otherexpences.UpdateOtherExpences(bl_field);
                Clear();
                MessageBox.Show("Record Updated Successfully");
                BindData();
            }
        }




        //=======================Delete=====================





        private void btn_Delete_Click(object sender, EventArgs e)
        {
            if (txtExpenseName.Text == "" || txtAmount.Text == "")
            {
                MessageBox.Show("Please Select Specific Record");
            }
            else
            {
                try
                {

                    DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Record?", "Delete", MessageBoxButtons.YesNo);
                    if (msgg == DialogResult.Yes)
                    {
                       
                        bl_field.OtherExpenseId = dgvShowOtherExpenseInfo.SelectedRows[0].Cells[0].Value.ToString();
                        bl_otherexpences.DeleteOtherExpences(bl_field);
                        MessageBox.Show("Record Delete Successfully");
                        BindData();
                        Clear();
                    }
                }
                catch
                {

                    MessageBox.Show("error occured!Please contact Administration?");
                }
            }
        }



        //=======================Cancel=====================




        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Clear();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;


        }



        //=======================Grid View=====================




        private void dgvShowOtherExpenseInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvShowOtherExpenseInfo.Rows.Count > 0)
            {

                bl_field.OtherExpenseId = dgvShowOtherExpenseInfo.SelectedRows[0].Cells[0].Value.ToString();
                dateTimePicker1.Text    = dgvShowOtherExpenseInfo.SelectedRows[0].Cells[1].Value.ToString();
               
                txtExpenseName.Text     = dgvShowOtherExpenseInfo.SelectedRows[0].Cells[3].Value.ToString();
                txtAmount.Text          = dgvShowOtherExpenseInfo.SelectedRows[0].Cells[4].Value.ToString();
                txtRemark.Text          = dgvShowOtherExpenseInfo.SelectedRows[0].Cells[5].Value.ToString();
            }


            btn_Save.Enabled = false;
            btn_Update.Enabled = true;
            btn_Delete.Enabled = true;

        }




        //=======================Form Load=====================



        private void OtherExpences_Load(object sender, EventArgs e)
        {
            //BindData();
          
        }

        //private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    //common c = new common();
        //    //c.NumberOnly(e);
        //}

        private void OtherExpences_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

       
    }
}
