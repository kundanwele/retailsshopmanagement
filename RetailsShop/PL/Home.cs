﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using RetailsShop.PL.Reports;



using System.Diagnostics;

namespace RetailsShop.PL
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
        }

        private void paymentModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddPaymentMode ap = new AddPaymentMode();
            ap.ShowDialog();
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void shopDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShopDetails od = new ShopDetails();
            od.ShowDialog();
        }

        private void createUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateUser cu = new CreateUser();
            cu.ShowDialog();
        }

        private void smsSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SMSsettimg ss = new SMSsettimg();
            ss.ShowDialog();
        }

        private void calculatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("calc.exe");
        }

        private void notePadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("notepad.exe");
        }

        private void expensesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OtherExpences oe = new OtherExpences();
            oe.ShowDialog();
            
        }

        private void sendSmsEmailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendSms sms = new SendSms();
            sms.ShowDialog();
        }

        private void loginHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void Home_Load(object sender, EventArgs e)
        {

        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            LogIn lg = new LogIn();
            lg.ShowDialog();
        }

        private void supplierDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Supplier sp = new Supplier();
            sp.ShowDialog();
        }

        private void workerDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Worker w = new Worker();
            w.ShowDialog();

        }

        private void emailSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmailSetting es = new EmailSetting();
            es.ShowDialog();
        }

        private void printerSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrinterSetup ps = new PrinterSetup();
            ps.ShowDialog();

        }

        private void itemAccessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ItemAccess ia = new ItemAccess();
            ia.ShowDialog();

        }

        private void menuAccessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MenuHead mh = new MenuHead();
            mh.ShowDialog();
        }

        private void customerDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Customer cm = new Customer();
            cm.ShowDialog();
        }

        private void deliveryBoyDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeliveryBoyDetails dl = new DeliveryBoyDetails();
            dl.ShowDialog();

        }

        private void manaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ManagerDetails md = new ManagerDetails();
            md.ShowDialog();

        }

        private void manufacturerDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Manufacturer m = new Manufacturer();
            m.ShowDialog();
        }

      

        private void taxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tax tx = new Tax();
            tx.ShowDialog();
        }

        private void saleStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sale s = new Sale();
            s.ShowDialog();
        }

        private void manageAccessLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccessLevel al = new AccessLevel();
            al.ShowDialog();
        }


        private void purchaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Purchasing p = new Purchasing();
            p.ShowDialog();
        }

        private void viewStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stock st = new Stock();
            st.ShowDialog();
        }

        private void addItemsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductName pn = new ProductName();
            pn.ShowDialog();

        }
        private void invoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sale iv = new Sale();
            iv.ShowDialog();

        }



        //=====================================================================================

        private void daywiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            Report2Date rd = new Report2Date("purchase");
            rd.ShowDialog();
        }

       
    }
}
