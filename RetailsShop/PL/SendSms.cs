﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using RetailsShop.PL;
using RetailsShop.BL;
using RetailsShop.DL;

using System.Data.SqlClient;


namespace RetailsShop.PL
{
    public partial class SendSms : Form
    {

        BL_SendSMS bl_sendsms = new BL_SendSMS();
        BL_Field bl_field = new BL_Field();


        public SendSms()
        {
            InitializeComponent();
        }



        private void BindData()
        {
            DataTable dt = bl_sendsms.SelectSendSms();
            dgvShowSendSmsInfo.DataSource = dt;

            dgvShowSendSmsInfo.Columns["SendSmsId"].Visible = false;
        }


        private void Clear()
        {
            txtTo.Text = "";
            txtMessage.Text = "";
        }


        private void SendSms_Load(object sender, EventArgs e)
        {
           // BindData();

        }


        //==========Save===========

        private void btn_Save_Click(object sender, EventArgs e)
        {
            if(txtTo.Text=="")
            {
                MessageBox.Show("You Can't leave To empty ");
            }
            else if(txtMessage.Text=="")
            {
                MessageBox.Show("You Can't leave Message empty ");
            }
            else
            {
            bl_field.SendTo = txtTo.Text;
            bl_field.Message = txtMessage.Text;

            bl_sendsms.InsertSendSms(bl_field);
            Clear();
            MessageBox.Show("Record Inserted Successfully");
            BindData();
            }

        }



        //==========Update===========


        private void btn_Update_Click(object sender, EventArgs e)
        {
            if(txtTo.Text=="" || txtMessage.Text=="")
            {
                MessageBox.Show("Please Select Specific Record");
            }
            else
            {
            bl_field.SendTo = txtTo.Text;
            bl_field.Message = txtMessage.Text;
            bl_sendsms.UpdateSendSms(bl_field);
            

            Clear();
            MessageBox.Show("Record Updated Successfully");
            BindData();
            }

        }


        //==========Delete===========



        private void btn_Delete_Click(object sender, EventArgs e)
        {
            if (txtTo.Text == "" || txtMessage.Text == "")
            {
                MessageBox.Show("Please Select Specific Record");
            }
            else
            {
                try
                {

                    DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Record?", "Delete", MessageBoxButtons.YesNo);
                    if (msgg == DialogResult.Yes)
                    {

                        bl_field.SendSmsId = dgvShowSendSmsInfo.SelectedRows[0].Cells[0].Value.ToString();
                        bl_sendsms.DeleteSendSms(bl_field);

                        MessageBox.Show("Record Delete Successfully");
                        BindData();
                        Clear();
                    }
                }
                catch
                {

                    MessageBox.Show("error occured!Please contact Administration?");
                }
            }

        }


        //=========Clear==========



        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Clear();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;


        }



        //==========Grid View===========



        private void dgvShowSendSmsInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvShowSendSmsInfo.Rows.Count > 0)
            {

                bl_field.SendSmsId = dgvShowSendSmsInfo.SelectedRows[0].Cells[0].Value.ToString();
                txtTo.Text         = dgvShowSendSmsInfo.SelectedRows[0].Cells[1].Value.ToString();
                txtMessage.Text    = dgvShowSendSmsInfo.SelectedRows[0].Cells[2].Value.ToString();
            }

            btn_Save.Enabled = false;
            btn_Update.Enabled = true;
            btn_Delete.Enabled = true;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void SendSms_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
