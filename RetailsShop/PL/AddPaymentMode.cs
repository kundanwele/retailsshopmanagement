﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using RetailsShop.BL;
using RetailsShop.PL;
using RetailsShop.DL;


using System.Data.SqlClient;


namespace RetailsShop.PL
{
    public partial class AddPaymentMode : Form
    {
        BL_AddPaymentMode bl_addpaymentmode = new BL_AddPaymentMode();


        public AddPaymentMode()
        {
            InitializeComponent();
        }



        //================BindData=========================



        private void BindData()
        {
            DataTable dt = bl_addpaymentmode.SelectPaymentMode();
            dgvShowPayModeInfo.DataSource = dt;

            dgvShowPayModeInfo.Columns["PaymentModeId"].Visible = false;
        }



        //================Clear=========================



        private void Clear()
        {
            txtPaymentMode.Text = "";
        }



        //================Form Load=========================



        private void AddPaymentMode_Load(object sender, EventArgs e)
        {
            BindData();

            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }



        //================Save=========================



        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (txtPaymentMode.Text == "")
            {
                MessageBox.Show("You can't leave Medium Name empty");
            }

            else
            {

                BL_Field bl_field = new BL_Field();
                bl_field.PaymentMode = txtPaymentMode.Text;

                bl_addpaymentmode.InsertPaymentMode(bl_field);
                Clear();
                MessageBox.Show("Record Inserted Successfully");
                BindData();
            }
        }



        //================Update=========================



        private void btn_Update_Click(object sender, EventArgs e)
        {
            if (txtPaymentMode.Text == "")
            {
                MessageBox.Show("Please Select Medium Name");
            }
            else
            {
                BL_Field bl_field = new BL_Field();
                bl_field.PaymentMode = txtPaymentMode.Text;
                bl_field.PaymentModeId = dgvShowPayModeInfo.SelectedRows[0].Cells[0].Value.ToString();

                bl_addpaymentmode.UpdatePaymentMode(bl_field);
                Clear();
                MessageBox.Show("Record Update Successfully");
                BindData();

                btn_Save.Enabled = true;
                btn_Update.Enabled = false;
                btn_Delete.Enabled = false;
            }
        }




        //================Delete=========================



        private void btn_Delete_Click(object sender, EventArgs e)
        {
            if (txtPaymentMode.Text == "")
            {
                MessageBox.Show("Please Select Medium Name");
            }

            else
            {
                try
                {

                    DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Payment Mode?", "Delete", MessageBoxButtons.YesNo);
                    if (msgg == DialogResult.Yes)
                    {
                        BL_Field bl_field = new BL_Field();
                        bl_field.PaymentModeId = dgvShowPayModeInfo.SelectedRows[0].Cells[0].Value.ToString();
                        bl_addpaymentmode.DeletePaymentMode(bl_field);
                        MessageBox.Show("Record Delete Successfully");
                        BindData();
                        Clear();
                    }
                }
                catch
                {

                    MessageBox.Show("error occured!Please contact Administration?");
                }


                btn_Save.Enabled = true;
                btn_Update.Enabled = false;
                btn_Delete.Enabled = false;
            }
        }



        //================Cancel=========================




        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Clear();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;


        }



        //================Grid View=========================




        private void dgvShowPayModeInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dgvShowPayModeInfo.Rows.Count>0)
            {
                BL_Field bl_field = new BL_Field();

                bl_field.PaymentModeId=dgvShowPayModeInfo.SelectedRows[0].Cells[0].Value.ToString();
                txtPaymentMode.Text= dgvShowPayModeInfo.SelectedRows[0].Cells[1].Value.ToString();

            }

            btn_Save.Enabled = false;
            btn_Update.Enabled = true;
            btn_Delete.Enabled =true;
        }

        private void AddPaymentMode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }


        }

    }
}
