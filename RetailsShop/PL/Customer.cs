﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;


namespace RetailsShop.PL
{
    public partial class Customer : Form
    {
        DL_Customer dl_customer = new DL_Customer();
        BL_Customer bl_customer = new BL_Customer();

        public Customer()
        {
            InitializeComponent();
        }



        private void SetField()
        {
            if (cmbType.Text == "Customer")
            {
                bl_customer.Type = "C";
            }
            if (cmbType.Text == "Distributor")
            {
                bl_customer.Type = "D";
            }
           bl_customer.FirstName = txtname.Text;
           bl_customer.MiddleName = txtmiddlename.Text;
           bl_customer.LastName = txtlastname.Text;
           bl_customer.PhoneNo = txtphone.Text;
           bl_customer.Mobile = txtmobileno.Text;
           bl_customer.Email = txtemail.Text;
           bl_customer.Country = txtcountry.Text;
           bl_customer.State = txtstate.Text;
           bl_customer.City = txtcity.Text;
           bl_customer.Address = txtaddress.Text;


        }


        private void ClearField()
        {
            cmbType.Text = "";
            txtname.Text = "";
            txtmiddlename.Text = "";
            txtlastname.Text = "";
            txtphone.Text = "";
            txtmobileno.Text = "";
            txtemail.Text = "";
            txtcountry.Text = "";
            txtstate.Text = "";
            txtcity.Text = "";
            txtaddress.Text = "";

        }


        private void BindCustomer()
        {
            DataTable dt =bl_customer.SelectCustomer();
            grdCustomer.DataSource = dt;
            grdCustomer.Columns["CustomerId"].Visible = false;
            grdCustomer.Columns["Status"].Visible = false;
        }

      

        private void btn_Save_Click(object sender, EventArgs e)
        {
            SetField();
            bl_customer.InsertCustomer(bl_customer);
            MessageBox.Show("Record Inserted successfully");
            ClearField();
            BindCustomer();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }


        private void btn_Update_Click(object sender, EventArgs e)
        {
            SetField();
            bl_customer.UpdateCustomer(bl_customer);
            MessageBox.Show("Record Updated successfully");
            ClearField();
            BindCustomer();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }


        private void btn_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_customer.DeleteCustomer(bl_customer);
                    ClearField();
                    MessageBox.Show("Record Deleted Successfully");
                    BindCustomer();

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
           
        }


        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            ClearField();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }



        private void grdCustomer_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           if(grdCustomer.Rows.Count > 0)
           {
                bl_customer.CustomerId = grdCustomer.SelectedRows[0].Cells[0].Value.ToString();
               // string a = "0";
                
                
                txtname.Text = grdCustomer.SelectedRows[0].Cells[1].Value.ToString();
                txtmiddlename.Text = grdCustomer.SelectedRows[0].Cells[2].Value.ToString();
                txtlastname.Text = grdCustomer.SelectedRows[0].Cells[3].Value.ToString();
                txtphone.Text = grdCustomer.SelectedRows[0].Cells[4].Value.ToString();
                txtmobileno.Text = grdCustomer.SelectedRows[0].Cells[5].Value.ToString();
                txtemail.Text = grdCustomer.SelectedRows[0].Cells[6].Value.ToString();
                txtcountry.Text = grdCustomer.SelectedRows[0].Cells[7].Value.ToString();
                txtstate.Text = grdCustomer.SelectedRows[0].Cells[8].Value.ToString();
                txtcity.Text = grdCustomer.SelectedRows[0].Cells[9].Value.ToString();
                txtaddress.Text = grdCustomer.SelectedRows[0].Cells[10].Value.ToString();

               // cmbType.Text = grdCustomer.SelectedRows[0].Cells[12].Value.ToString();
                if (grdCustomer.SelectedRows[0].Cells[12].Value.ToString() == "C")
                {
                    cmbType.Text = "Customer";

                }
                else
                {
                    cmbType.Text = "Distributor";
                }

                    
           }

            

            btn_Save.Enabled = false;
            btn_Update.Enabled = true;
            btn_Delete.Enabled = true;
        }

        private void txtphone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;

            }
        }

        private void txtmobileno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;

            }
        }

        private void Customer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void Customer_Load(object sender, EventArgs e)
        {
            BindCustomer();

            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }

        private void cmbType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void grdCustomer_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

 
    }
}
