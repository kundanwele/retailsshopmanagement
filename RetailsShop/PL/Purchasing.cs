﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using RetailsShop.PL.Reports;
using System.Globalization;

namespace RetailsShop.PL
{
    public partial class Purchasing : Form
    {

        BL_Purchase bl_purchase = new BL_Purchase();
        BL_Vendor bl_vendor = new BL_Vendor();

        DataSet dspurchase = new DataSet();
        DataTable dt = new DataTable();


        public Purchasing()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

       
            //try
            //{

                //if (btnAdd.Text == "Add")
                //{
                  
                //        SetField();
                //     string id= bl_purchase.InsertPurchase(bl_purchase);
                //     bl_purchase.PurchaseId = Convert.ToInt32(id);
                    
                    // int newstock = 0;
                    //if (dt.Rows.Count > 0)
                    //    {
                    //        for (int i = 0; i < dt.Rows.Count;i++ )
                    //        {
                    //            //bl_field.SubcatId = dt.Rows[i][0].ToString();
                    //            bl_purchase.ProductNameId = dt.Rows[i][0].ToString();
                    //            bl_purchase.Qty = dt.Rows[i][2].ToString();
                                
                    //            bl_purchase.Date = dt.Rows[i][6].ToString();
                               
                    //            bl_purchase.Rate = dt.Rows[i][8].ToString();
                    //            if (dt.Rows[i][9].ToString() != "")
                    //            {
                    //                bl_purchase.Tax = dt.Rows[i][9].ToString();
                    //            }
                    //            else {
                    //                bl_purchase.Tax = "0";
                    //            }
                    //            if (dt.Rows[i][10].ToString() != "")
                    //            {
                    //                bl_purchase.TaxAmt = dt.Rows[i][10].ToString();
                    //            }
                    //            else {
                    //                bl_purchase.TaxAmt = "0"; 
                    //            }
                    //            bl_purchase.Total = Convert.ToDecimal(dt.Rows[i][11].ToString());
                    //            bl_purchase.InsertPurchaseItem(bl_purchase);

                           
                        //Clear();
                        //dt.Clear();
                        //MessageBox.Show("Recods Inserted Successfully");
                     
          
                
                //else
                //{
                //    SetField();
                //    bl_purchase.UpdatePurchase(bl_purchase);
                //    Clear();
                //   // BindPurchase();
                //    btnAdd.Text =

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            if(btnAdd.Text=="Add")

            {
                SetField();
                bl_purchase.InsertPurchaseDetail(bl_purchase);
                SelectPurchaseDetail();
               CalculateSubTotal();
               // Clear();
                txtProductName.Focus();



            }
        
            else
            {
                 SetField();
                bl_purchase.UpdatePurchaseDetail(bl_purchase);
                SelectPurchaseDetail();
                CalculateSubTotal();
                //Clear();

                btnAdd.Text="Add";

              

            }
            


        }

        //private void BindProductName()
        //{
        //    DataTable dt = bl_productname.SelectProductName();
        //    if (dt.Rows.Count > 0)
        //    {
        //        ddlitem.DataSource = dt;
        //        ddlitem.DisplayMember = "ProductName";
        //        ddlitem.ValueMember = "ProductNameId";
        //    }
        //    else
        //    {
        //        ddlitem.DataSource = null;
        //    }
        //}

        private void SetField()
        {
             
             bl_purchase.Date = dtpPurchaseDate.Text;
            bl_purchase.InvoiceNo = txtInvoiceNo.Text;
            bl_purchase.SupplierId=cmbSuplName.SelectedValue.ToString();
            bl_purchase.ManufactureId = cmbManufName.SelectedValue.ToString();
           // bl_purchase.ProductNameId=grdProduct.SelectedRows[0].Cells[1].Value.ToString();
            bl_purchase.ProductNameId = txtProductName.Text;
            bl_purchase.SerialNo = txtSerialNo.Text;
            bl_purchase.Qty = txtQty.Text;
            bl_purchase.Rate = txtRate.Text;
            bl_purchase.Discount = txtDiscount.Text;
            bl_purchase.Vat = Taxlabel.Text;

             
          
          
               
        }
        //private void Clear()
        //{
        //    dtpPurchaseDate.Text = "";
        //    txtInvoiceNo.Text="";
        //    cmbManufName.Text = "";
        //    cmbSuplName.Text = "";
        //    txtSerialNo.Text = "";            
        //    txtQty.Text = "";
        //    txtRate.Text = "";           
        //    txtDiscount.Text = "";
           
          
            


        //}
        private void Purchasing_Load(object sender, EventArgs e)
        {

            //Common common = new Common();
            //Taxlabel.Text = common.ReturnOneValue("select TaxRate from Tax");



            //dtpPurchaseDate.CustomFormat = "dd/MM/yyyy";
            //grdProduct.Visible = false;

            //BindSupplier();
            //BindManufact();

            //dt.Clear();

            //dt.Columns.Add("ProductNameId");
            //dt.Columns.Add("ProductName");
            //dt.Columns.Add("Qty");
            //dt.Columns.Add("Size");
            //dt.Columns.Add("TabQty");
            //dt.Columns.Add("BatchNo");
            //dt.Columns.Add("PDate");
            //dt.Columns.Add("ExpDate");
            //dt.Columns.Add("Rate");
            //dt.Columns.Add("TaxPer");
            //dt.Columns.Add("TaxAmt");
            //dt.Columns.Add("Amount");
     
            
        }

        private void BindSupplier()
        {
            DataTable dt = bl_vendor.SelectVendor();
            if (dt.Rows.Count > 0)
            {
                cmbSuplName.DataSource = dt;
                cmbSuplName.ValueMember = "SupplierId";
                cmbSuplName.DisplayMember = "SupplierName";

            }

        }


        private void BindManufact()
        {
            DataTable dt = bl_purchase.SelectManufact();
            if (dt.Rows.Count > 0)
            {
                cmbManufName.DataSource = dt;
                cmbManufName.ValueMember = "ManufactureId";
                cmbManufName.DisplayMember = "ManufactureName";

            }

        }



      
        //private void btndelete_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
        //        if (msgg == DialogResult.Yes)
        //        {
        //            bl_purchase.PurchaseId = Convert.ToInt32(grdPurchase.SelectedRows[0].Cells[0].Value.ToString());
        //            bl_purchase.DeletePurchase(bl_purchase);
        //            Clear();
        //        }
        //    }
        //    catch
        //    {

        //    }






        //}


        

      

       
        


        

        
       
       
        
      

        //private void txtitemname_TextChanged(object sender, EventArgs e)
        //{

        //    if (txtProductName.Text != "")
        //    {

        //        if (txtProductName.Focused)
        //        {
        //            bl_purchase.TextSearch = txtProductName.Text;
        //            DataTable dt = bl_purchase.SelectProductNameBySearch(bl_purchase);
        //            if (dt.Rows.Count > 0)
        //            {
        //                grdProduct.Visible = true;
        //                grdProduct.DataSource = dt;
        //                grdProduct.Columns["ProductNameId"].Visible = false;
        //                grdProduct.Columns["CompanyName"].Visible = false;
        //                grdProduct.Columns["ReorderLevel"].Visible = false;
        //                grdProduct.Columns["Status"].Visible = false;
        //                grdProduct.Columns["ProductType"].Visible = false;
        //                grdProduct.Columns["Tax"].Visible = false;
          

        //            }
        //        }
        //    }
        //    else
        //    {
        //        grdProduct.Visible = false;
        //        Clear();
        //    }



        //}

        //private void txtitemname_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        if (txtProductName.Text != "")
        //        {
        //            txtQty.Focus();
        //            txtProductName.Text = grdProduct.SelectedRows[0].Cells[1].Value.ToString();
                   
        //            txtRate.Text = grdProduct.SelectedRows[0].Cells[7].Value.ToString();
        //            //txtTax.Text = grdProduct.SelectedRows[0].Cells[9].Value.ToString();
        //            //if (grditemName.SelectedRows[0].Cells[9].Value.ToString() != "")
        //            //{
        //            //    txttaxamt.Text = Convert.ToString(Convert.ToInt32(txtitemtotal.Text) * Convert.ToInt32(txttax.Text) / 100);
        //            //}
        //            grdProduct.Visible = false;
                 
        //        }
        //        else {
        //            txtProductName.Text = "";
                  
        //            txtRate.Text = "";
        //        }


        //    }

        //    if (e.KeyCode == Keys.Up)
        //    {
        //        DataTable dtTemp = grdProduct.DataSource as DataTable;

        //        object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
        //        for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
        //        {
        //            dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
        //        }
        //        dtTemp.Rows[0].ItemArray = arr;



        //    }

        //    if (e.KeyCode == Keys.Down)
        //    {

        //        DataTable dtTemp = grdProduct.DataSource as DataTable;

        //        object[] arr = dtTemp.Rows[0].ItemArray;
        //        for (int i = 1; i < dtTemp.Rows.Count; i++)
        //        {
        //            dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
        //        }
        //        dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;



        //    }
        //}

       

        //private void btnAdd_Click_1(object sender, EventArgs e)
        //{
        //    if (btnAdd.Text == "Add")
        //    {
        //        SetField();

        //        bl_purchase.InsertPurchase(bl_purchase);
        //        CalculateSubTotal();


        //    }

        //    else
        //    {
        //        SetField();

        //        bl_purchase.UpdatePurchase(bl_purchase);


        //    }
        //}



        //private void CalculateSubTotal()
        //{
        //    int qty ;
        //    //if (txtQty.Text == "")
        //    //{
        //    //    qty = 1;
        //    //}
        //    //else 
        //    //{
        //    //    qty = Convert.ToInt32(txtQty.Text);
        //    //}

        //    int rate = 0;
        //    rate = Convert.ToInt32(rate);

        //    double mul = 0;
        //    mul = Convert.ToDouble(mul);

        //    int dis = 0;

        //    double disval = 0;
        //    disval = Convert.ToDouble(disval);

        //    double sub = 0;
        //    sub = Convert.ToDouble(sub);

        //    double ftotal = 0;
        //    ftotal = Convert.ToDouble(ftotal);


        //    for (int i = 0; i < grdPurchase.Rows.Count; i++)
        //    {
        //        qty = Convert.ToInt32(grdPurchase.Rows[i].Cells[4].Value);
        //        rate = Convert.ToInt32(grdPurchase.Rows[i].Cells[5].Value);

        //        if (qty.ToString() == "")
        //        {
        //            qty = 1;
        //        }
        //        else 
        //        {
        //            qty = Convert.ToInt32(grdPurchase.Rows[i].Cells[5].Value);
        //        }

        //        mul = qty * rate;

        //        dis = Convert.ToInt32(grdPurchase.Rows[i].Cells[6].Value);
        //        if (dis.ToString() == "")
        //        {
        //            dis = 0;
        //        }
        //        else
        //        {
        //            dis = Convert.ToInt32(grdPurchase.Rows[i].Cells[6].Value);
        //        }
               

        //        if (dis != 0)
        //        {
        //            //dis= Convert.ToInt32(grdSale.Rows[i].Cells[4].Value);

        //            disval = mul * dis / 100;
        //            sub = mul - disval;
        //            //txtAmount.Text = sub.ToString();
        //            ftotal = ftotal + sub;

        //        }

        //        else
        //        {
        //            //txtAmount.Text = mul.ToString();
        //            ftotal = ftotal + mul;
        //        }
        //    }

        //}

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Customer?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {

                    
                    bl_purchase.PurchaseDetailId = Convert.ToInt32(grdPurchase.SelectedRows[0].Cells[0].Value.ToString());
                    bl_purchase.DeletePurchaseDetail(bl_purchase);
                    MessageBox.Show("Record Delete Successfully");
                    SelectPurchaseDetail();
                    CalculateSubTotal();
                    //Clear();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }

        }

        private void SelectPurchaseDetail()
        {
            DataTable dt=bl_purchase.SelectPurchaseDetail(bl_purchase);
            grdPurchase.DataSource=dt;

            grdPurchase.Columns["PurchaseDetailId"].Visible = false;
            grdPurchase.Columns["PurchaseId"].Visible = false;
            grdPurchase.Columns["Status"].Visible = false;



        }

        private void Purchasing_Load_1(object sender, EventArgs e)
        {
            //Common common = new Common();
            //Taxlabel.Text = common.ReturnOneValue("select TaxRate from Tax");

            SelectPurchaseDetail();
            Common common = new Common();
            Taxlabel.Text = common.ReturnOneValue("Select TaxRate from Tax");
            BindManufact();
            BindSupplier();


            grdProduct.Visible = false;
        }





        private void txtProductName_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                if (grdProduct.Rows.Count > 0)
                {
                    txtProductName.Text = "";
                    txtProductName.Text = grdProduct.SelectedRows[0].Cells[1].Value.ToString();
                    //bl_field.StudentstaticId = grdperson.SelectedRows[0].Cells[0].Value.ToString();
                    grdProduct.Hide();

                    txtSerialNo.Focus();
                }
                else
                {
                    grdProduct.Visible = false;
                    txtProductName.Focus();
                }

            }








            
        }





        private void txtSerialNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtQty.Focus();

            }
        }

        private void txtQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtRate.Focus();

            }
        }

        private void txtRate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtDiscount.Focus();

            }

        }

        private void txtDiscount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtAmount.Focus();

            }
        }

        private void txtAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAdd.Focus();

            }
        }

        private void btnAdd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtProductName.Focus();

            }

        }

        private void grdPurchase_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                btnAdd.Text = "Update";

                bl_purchase.PurchaseDetailId = Convert.ToInt32(grdPurchase.SelectedRows[0].Cells[0].Value.ToString());
                bl_purchase.PurchaseId = Convert.ToInt32(grdPurchase.SelectedRows[0].Cells[1].Value.ToString());
                bl_purchase.ProductNameId = grdPurchase.SelectedRows[0].Cells[2].Value.ToString();

                
                txtSerialNo.Text = grdPurchase.SelectedRows[0].Cells[3].Value.ToString();
                txtQty.Text = grdPurchase.SelectedRows[0].Cells[4].Value.ToString();
                txtRate.Text = grdPurchase.SelectedRows[0].Cells[5].Value.ToString();
                txtDiscount.Text = grdPurchase.SelectedRows[0].Cells[6].Value.ToString();

                CalculateAmount();


            }
            catch 
            {
                MessageBox.Show("error occured!Please contact Administration?");
            }

        }


        private void CalculateAmount()
        {
            int q = 0;
            if(txtQty.Text=="")
            {
             q = 1;
            }
            else
            {
                q = Convert.ToInt32(txtQty.Text); 
            }


            double r = 0;
            r = Convert.ToDouble(txtRate.Text);

            double m = 0;
            m = Convert.ToDouble(m);

            int d = 0;
            if (txtDiscount.Text == "")
            {
                d = 0;
            }
            else
            {
                d = Convert.ToInt32(txtDiscount.Text);
            }

            double dv = 0;
            dv = Convert.ToDouble(dv);

            double s = 0;
            s = Convert.ToDouble(s);


            m = r * q;

            if (d != 0)
            {
                dv = m * d / 100;
                s = m - dv;

                txtAmount.Text = s.ToString();

            }
            else
            {
                txtAmount.Text = m.ToString();
            }

        }

       

        private void txtQty_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            Common c = new Common();
            c.NumberOnly(e);

        }

        private void txtRate_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            Common c = new Common();
            c.NumberOnly(e);
        }

        private void txtDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common c = new Common();
            c.NumberOnly(e);
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common c = new Common();
            c.NumberOnly(e);
        }

        private void txtSerialNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common c = new Common();
            c.NumberOnly(e);
        }

        private void txtDiscount_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtAmount.Focus();

            }

        }

        private void txtDiscount_TextChanged(object sender, EventArgs e)
        {
            CalculateAmount();
        }

        private void txtRate_TextChanged(object sender, EventArgs e)
        {
            CalculateAmount();
        }





        private void CalculateSubTotal()
        {
            int qty = 0;

            double rate = 0;
            rate = Convert.ToDouble(rate);

            double mul = 0;
            mul = Convert.ToDouble(mul);

            int dis = 0;

            double disval = 0;
            disval = Convert.ToDouble(disval);

            double sub = 0;
            sub = Convert.ToDouble(sub);

            double ftotal = 0;
            ftotal = Convert.ToDouble(ftotal);


            for (int i = 0; i < grdPurchase.Rows.Count; i++)
            {
                qty = Convert.ToInt32(grdPurchase.Rows[i].Cells[4].Value);
                rate = Convert.ToInt32(grdPurchase.Rows[i].Cells[5].Value);

                mul = qty * rate;


                dis = Convert.ToInt32(grdPurchase.Rows[i].Cells[6].Value);

                if (dis != 0)
                {
                    //dis= Convert.ToInt32(grdSale.Rows[i].Cells[4].Value);

                    disval = mul * dis / 100;
                    sub = mul - disval;
                    //txtAmount.Text = sub.ToString();
                    ftotal = ftotal + sub;

                }

                else
                {
                    //txtAmount.Text = mul.ToString();
                    ftotal = ftotal + mul;
                }
            }

            //txtSubTotal.Text = ftotal.ToString();

          
                double tax = 0;
                tax = Convert.ToDouble(Taxlabel.Text);

                double taxVal = 0;
                taxVal = Convert.ToDouble(taxVal);

                double taxAmt = 0;
                taxAmt = Convert.ToDouble(taxAmt);

                tax = tax / 100;
                taxVal = ftotal * tax;
                txtAmt.Text = taxVal.ToString();

                taxAmt = ftotal + taxVal;
                txtTotal.Text = taxAmt.ToString();
           
           
            






        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            SetField();
            InsertPurchase();

        }

        private void InsertPurchase()
        {
            bl_purchase.InsertPurchase(bl_purchase);
        }

        private void txtProductName_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = null;
            if (txtProductName.Text != "")
            {
                
                //bl_field.TextSearch = txtsearch.Text;
                //dt = dl_reports.SelectStudent(bl_field);

                bl_purchase.ProductName = txtProductName.Text;
                dt = bl_purchase.SelectProduct(bl_purchase);

                if (dt.Rows.Count > 0)
                {
                    grdProduct.Visible = true;
                    grdProduct.DataSource = dt;
                    grdProduct.Columns["ProductNameId"].Visible = false;
                    grdProduct.Columns["Status"].Visible = false;
                }

            }
            else
            {
                if (txtProductName.Focused)
                {
                    grdProduct.Visible = true;
                    dt = bl_purchase.SelectAllProduct();
                    grdProduct.DataSource = dt;
                }


            }
        }










    }
}
