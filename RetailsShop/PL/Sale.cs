﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using RetailsShop.PL.Reports;
using System.Globalization;


namespace RetailsShop.PL
{
    public partial class Sale : Form
    {
        DL_Sale dl_sale = new DL_Sale();
        BL_Sale bl_sale = new BL_Sale();
        Common common = new Common();
        BL_Field bl_field = new BL_Field();
        BL_ProductName bl_productname = new BL_ProductName();
        BL_Customer bl_customer = new BL_Customer();
        DataSet dssale = null;
        public Sale()
        {
            InitializeComponent();
        }


        private void SetField()
        {


            if (pnltax.Visible == true)
            {
                bl_sale.Vat = lbltax.Text;
            }
            else {
                bl_sale.Vat = "0";
            }
            bl_sale.Status = "Open";
            bl_sale.CustomerId = Convert.ToString(bl_customer.CustomerIdStatic);
            bl_sale.Date = dtpSaleDate.Text;
            bl_sale.ProductId = Convert.ToString(bl_productname.ProductNameIdStatic);
            bl_sale.SerialNo = txtSerialNo.Text;
            bl_sale.Qty = txtQty.Text;
            bl_sale.Rate = txtRate.Text;
            bl_sale.Discount = txtDiscount.Text;

        }

        private void Clear()
        {
            dtpSaleDate.Text = "";
            txtProduct.Text  = "";
            txtSerialNo.Text = "";
            txtQty.Text      = "";
            txtRate.Text     = "";
            txtDiscount.Text = "";
            txtAmount.Text   = "";

        }

        private void BindData()
        {
 
        }


        private void Invoice_Load(object sender, EventArgs e)
        {
           
            grdCustomer.Visible = false;
            grdproduct.Visible = false;
            grpbInstlDet.Visible = false;

            txttaxAmt.Enabled = false;
            Common common = new Common();
            lbltax.Text=common.ReturnOneValue("select TaxRate from Tax");
            rdounvatable.Checked = true;
            SelectOrderDetails();
        }

        private void txtCustomer_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = null;


            if (txtCustomer.Text != "")
            {

                bl_sale.TextSearch = txtCustomer.Text;

                dt = bl_sale.SelectCustomer(bl_sale);

                if (dt.Rows.Count > 0)
                {
                    grdCustomer.Visible = true;
                    grdCustomer.DataSource = dt;
                    grdCustomer.Columns["CustomerId"].Visible = false;
                }

            }
            else
            {
                if (txtCustomer.Focused)
                {
                    grdCustomer.Visible = true;
                    dt = bl_sale.SelectCustomerAll();
                    grdCustomer.DataSource = dt;
                }


            }
        


        }

        private void txtCustomer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (grdCustomer.Rows.Count > 0)
                {
                   
                    txtCustomer.Text = grdCustomer.SelectedRows[0].Cells[1].Value.ToString();
                    bl_customer.CustomerIdStatic = Convert.ToInt32(grdCustomer.SelectedRows[0].Cells[0].Value.ToString());
                    grdCustomer.Hide();

                    txtProduct.Focus();
                }
                else
                {
                    grdCustomer.Visible = false;
                    txtCustomer.Focus();
                }

            }


            if (e.KeyCode == Keys.Up)
            {
                DataTable dtTemp = grdCustomer.DataSource as DataTable;
               

                object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
                for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
                {
                    dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
                }
                dtTemp.Rows[0].ItemArray = arr;



            }

            if (e.KeyCode == Keys.Down)
            {

                DataTable dtTemp = grdCustomer.DataSource as DataTable;

                object[] arr = dtTemp.Rows[0].ItemArray;
                for (int i = 1; i < dtTemp.Rows.Count; i++)
                {
                    dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
                }
                dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;



            }


        }

        private void txtProduct_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (grdproduct.Rows.Count > 0)
                {

                    txtProduct.Text = grdproduct.SelectedRows[0].Cells[1].Value.ToString();
                    txtRate.Text = grdproduct.SelectedRows[0].Cells[3].Value.ToString();
                    bl_productname.ProductNameIdStatic = Convert.ToInt32(grdproduct.SelectedRows[0].Cells[0].Value.ToString());
                    grdproduct.Hide();

                    txtProduct.Focus();
                }
                else
                {
                    grdproduct.Visible = false;
                    txtProduct.Focus();
                }

            }


            if (e.KeyCode == Keys.Up)
            {
                DataTable dtTemp = grdproduct.DataSource as DataTable;


                object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
                for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
                {
                    dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
                }
                dtTemp.Rows[0].ItemArray = arr;



            }

            if (e.KeyCode == Keys.Down)
            {

                DataTable dtTemp = grdproduct.DataSource as DataTable;

                object[] arr = dtTemp.Rows[0].ItemArray;
                for (int i = 1; i < dtTemp.Rows.Count; i++)
                {
                    dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
                }
                dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;



            }

        }

        private void txtQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtRate.Focus();

            }

        }

        private void txtRate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtDiscount.Focus();
            
            }
        }

        private void txtSerialNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtQty.Focus();

            }


        }


       

        private void txtAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                InsertOrderDetails();
                SelectOrderDetails();
                txtProduct.Focus();
            }



               
  
        }

        private void InsertOrderDetails()
        {
            SetField();

           bl_sale.InsertOrderDetails(bl_sale);
        }




        private void SelectOrderDetails()
        {
            dssale = bl_sale.SelectOrderDetails(bl_sale);
            grdSale.DataSource = dssale.Tables[0];
            btnAdd.Text = "Add";
            CalculateSubTotal();
            //grdSale.Columns["OrderDetailId"].Visible = false;
            //grdSale.Columns["OrderId"].Visible = false;
            //grdSale.Columns["Status"].Visible = false;
            //grdInvoice.Columns[""].Visible = false;
        }



        private void txtQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common c = new Common();
            c.NumberOnly(e);

        }

        private void txtDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common c = new Common();
            c.NumberOnly(e);
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common c = new Common();
            c.NumberOnly(e);
        }

        private void txtInstal_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common c = new Common();
            c.NumberOnly(e);
        }

        private void txtRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common c = new Common();
            c.NumberOnly(e);

        }

        private void rdbUnvat_MouseClick(object sender, MouseEventArgs e)
        {
            //txtAmt.Enabled = false;

            
        }

        private void rdbVat_MouseClick(object sender, MouseEventArgs e)
        {
            //grpbTaxAmt.Enabled = true;
        }

        private void rdbCredit_MouseClick(object sender, MouseEventArgs e)
        {
            grpbInstlDet.Show();
        }

        private void rdbCash_MouseClick(object sender, MouseEventArgs e)
        {
           
            grpbInstlDet.Hide();
        }

       

        private void dtpNextpay_MouseEnter(object sender, EventArgs e)
        {
           
                txtSubTotal.Focus();
 
            

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
           

        }

        private void grdSale_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnAdd.Text = "Update";

            bl_sale.OrderDetailId = grdSale.SelectedRows[0].Cells[0].Value.ToString();
            bl_sale.OrderId       = grdSale.SelectedRows[0].Cells[1].Value.ToString();

            txtProduct.Text  = grdSale.SelectedRows[0].Cells[2].Value.ToString();
            txtSerialNo.Text = grdSale.SelectedRows[0].Cells[3].Value.ToString();
            txtRate.Text     = grdSale.SelectedRows[0].Cells[4].Value.ToString();
            txtQty.Text      = grdSale.SelectedRows[0].Cells[5].Value.ToString();           
            txtDiscount.Text = grdSale.SelectedRows[0].Cells[6].Value.ToString();

                  

        }


        private void CalculateSubTotal()
        {
            //int qty = 0;            

            //double rate = 0;
            //rate = Convert.ToDouble(rate);

            //double mul = 0;
            //mul = Convert.ToDouble(mul);

            // int dis = 0;

            // double disval = 0;
            // disval = Convert.ToDouble(disval);

            // double sub = 0;
            // sub = Convert.ToDouble(sub);

            // double ftotal = 0;
            // ftotal = Convert.ToDouble(ftotal);


            // for (int i = 0; i < grdSale.Rows.Count; i++)
            // {
            //     qty =  Convert.ToInt32(grdSale.Rows[i].Cells[5].Value);
            //     rate = Convert.ToInt32(grdSale.Rows[i].Cells[4].Value);

            //     mul = qty * rate;
                

            //     dis = Convert.ToInt32(grdSale.Rows[i].Cells[6].Value);

            //     if (dis != 0)
            //     {
            //         //dis= Convert.ToInt32(grdSale.Rows[i].Cells[4].Value);

            //         disval = mul * dis / 100;
            //         sub = mul - disval;
            //         //txtAmount.Text = sub.ToString();
            //         ftotal = ftotal + sub;

            //     }

            //     else 
            //     {
            //         //txtAmount.Text = mul.ToString();
            //         ftotal = ftotal + mul;
            //     }
            // }
            
            // txtSubTotal.Text = ftotal.ToString();

            // if (rdbVat.Checked == true)
            // {
            //     txtAmt.Enabled = true;
            //     double tax = 0;
            //     tax = Convert.ToDouble(taxlabel.Text);

            //     double taxVal = 0;
            //     taxVal = Convert.ToDouble(taxVal);

            //     double taxAmt = 0;
            //     taxAmt = Convert.ToDouble(taxAmt);

            //     tax = tax / 100;
            //     taxVal = ftotal * tax;
            //     txtAmt.Text = taxVal.ToString();

            //     taxAmt = ftotal + taxVal;
            //     txtTotal.Text = taxAmt.ToString();
            // }
            // else 
            // {
            //     txtTotal.Text = ftotal.ToString();
            // }

            if (rdounvatable.Checked == true)
            {
                decimal total = 0;
                decimal subtotal = 0;
                for (int i = 0; i < grdSale.Rows.Count; i++)
                {
                    if (Convert.ToString(grdSale.Rows[i].Cells[4].Value) != "0")
                    {
                        decimal Amount = Convert.ToDecimal(grdSale.Rows[i].Cells["Qty"].Value) * Convert.ToDecimal(grdSale.Rows[i].Cells["Rate"].Value);
                        decimal disamt = (Amount) * Convert.ToDecimal(grdSale.Rows[i].Cells["Discount"].Value) / 100;
                        total = Amount - disamt;
                    }
                    else
                    {
                        total = Convert.ToDecimal(grdSale.Rows[i].Cells["Qty"].Value) * Convert.ToDecimal(grdSale.Rows[i].Cells["Rate"].Value);
                    }

                    subtotal = subtotal + total;
                }
                txtSubTotal.Text = Convert.ToString(subtotal);
                txtTotal.Text = Convert.ToString(subtotal);
            }
            else
            {
                decimal total = 0;
                decimal subtotal = 0;
                for (int i = 0; i < grdSale.Rows.Count; i++)
                {
                    if (Convert.ToString(grdSale.Rows[i].Cells[4].Value) != "0")
                    {
                        decimal Amount = Convert.ToDecimal(grdSale.Rows[i].Cells["Qty"].Value) * Convert.ToDecimal(grdSale.Rows[i].Cells["Rate"].Value);
                        decimal disamt = (Amount) * Convert.ToDecimal(grdSale.Rows[i].Cells["Discount"].Value) / 100;
                        total = Amount - disamt;
                    }
                    else
                    {
                        total = Convert.ToDecimal(grdSale.Rows[i].Cells["Qty"].Value) * Convert.ToDecimal(grdSale.Rows[i].Cells["Rate"].Value);
                    }

                    subtotal = subtotal + total;
                }
                txtSubTotal.Text = Convert.ToString(subtotal);
                //txttaxAmt.Text = Convert.ToString((subtotal) - Convert.ToDecimal(txttaxAmt.Text));
                //decimal taxamt = Convert.ToString((subtotal) - ((subtotal) * (Convert.ToDecimal(lbltax.Text))) / 100);
                txttaxAmt.Text=Convert.ToString((subtotal) * (Convert.ToDecimal(lbltax.Text)) / 100);
                txtTotal.Text = Convert.ToString((subtotal) - ((subtotal) * (Convert.ToDecimal(lbltax.Text))) / 100);
                
            }

          
        }

        private void CalculateItemAmount()
        {
            //int q = 0;
            //q = Convert.ToInt32(txtQty.Text);


            //double r = 0;
            //r = Convert.ToDouble(txtRate.Text);

            //double m = 0;
            //m = Convert.ToDouble(m);

            //int d = 0;
            //d=Convert.ToInt32( txtDiscount.Text);

            //double dv = 0;
            //dv = Convert.ToDouble(dv);

            //double s = 0;
            //s = Convert.ToDouble(s);


            //m = r * q;

            //if (d!= 0)
            //{
            //    dv = m * d / 100;
            //    s= m - dv;

            //    txtAmount.Text = Convert.ToString(Convert.ToDouble(s.ToString()));

            //}
            //else 
            //{
            //    txtAmount.Text = Convert.ToString(Decimal.Parse(m,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture));
            //}

            if (txtQty.Text != "" && txtRate.Text != "")
            {
                txtAmount.Text = Convert.ToString(Convert.ToDecimal(txtQty.Text) * Convert.ToDecimal(txtRate.Text));
            }
            if (txtDiscount.Text != "")
            {
                decimal disamt = Convert.ToDecimal(txtAmount.Text) * Convert.ToDecimal(txtDiscount.Text) / 100;
                txtAmount.Text = Convert.ToString(Convert.ToDecimal(txtAmount.Text) - (disamt));
            }
 
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Customer?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {

                    
                    bl_sale.OrderDetailId = grdSale.SelectedRows[0].Cells[0].Value.ToString();
                    bl_sale.DeleteOrderDetails(bl_sale);
                    MessageBox.Show("Record Delete Successfully");
                    SelectOrderDetails();
                    CalculateSubTotal();
                    Clear();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }

        }

        private void grdCustomer_Leave(object sender, EventArgs e)
        {
            grdCustomer.Visible = false;
        }

        private void grpbTaxAmt_Enter(object sender, EventArgs e)
        {

        }

        private void rdbVat_MouseClick_1(object sender, MouseEventArgs e)
        {
            txttaxAmt.Enabled = true;
        }

        private void radioButton2_MouseClick(object sender, MouseEventArgs e)
        {
            txttaxAmt.Enabled = false;
        }

        private void btnPrintBill_Click(object sender, EventArgs e)
        {
           DialogResult msgg = MessageBox.Show("Are you sure you want to Print Bill?", "Bill", MessageBoxButtons.YesNo);
           if (msgg == DialogResult.Yes)
           {
               SetField();
               string invoiceno = Convert.ToString(common.BillNo("select max(InvoiceNo) from Orders"));

               bl_sale.InvoiceNo = invoiceno;
              
               string id = bl_sale.InsertOrder(bl_sale);
               PrintBill(invoiceno);
               bl_sale.OrderId = id;
               bl_sale.Status = "Open";
               bl_sale.UpdateOrderDetails(bl_sale);
               SelectOrderDetails();
               Clear();
           }
    
        }

        private void PrintBill(string InvoiceNo)
        {

            if (rdounvatable.Checked == true)
            {
                string pp = common.ReturnOneValue("select BillPrinter from PrinterSetup");
                rptUnvatable rptdata = new rptUnvatable();
                rptdata.SetDataSource(dssale.Tables[0]);
                // rptdata.SetParameterValue("HeaderName", "Purchase Reports");
                rptdata.SetParameterValue("OrgName", bl_field.OrgName);
                rptdata.SetParameterValue("OrgAddress", bl_field.OrgAddress);

                rptdata.SetParameterValue("CustomerName", txtCustomer.Text);
                rptdata.SetParameterValue("Date", dtpSaleDate.Text);
                rptdata.SetParameterValue("InvoiceNo", InvoiceNo);

                DialogResult msgg = MessageBox.Show("Print Bill?", "Bill", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {

                    rptdata.PrintOptions.PrinterName = pp;
                    rptdata.PrintToPrinter(1, false, 0, 0);
                }


            }
            else
            {
                string pp = common.ReturnOneValue("select BillPrinter from PrinterSetup");
                rptVatable rptdata = new rptVatable();
                rptdata.SetDataSource(dssale.Tables[0]);
                rptdata.SetParameterValue("OrgName", bl_field.OrgName);
                rptdata.SetParameterValue("OrgAddress", bl_field.OrgAddress);

                rptdata.SetParameterValue("CustomerName", txtCustomer.Text);
                rptdata.SetParameterValue("Date", dtpSaleDate.Text);
                rptdata.SetParameterValue("InvoiceNo", InvoiceNo);

                rptdata.SetParameterValue("SubTotal", txtSubTotal.Text);
                rptdata.SetParameterValue("VatAmount", txttaxAmt.Text);
                rptdata.SetParameterValue("FinalTotal", txtTotal.Text);
                string word = common.changeToWords(Convert.ToString(txtTotal.Text), true);
                rptdata.SetParameterValue("word", word);

                DialogResult msgg = MessageBox.Show("Print Bill?", "Bill", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {

                    rptdata.PrintOptions.PrinterName = pp;
                    rptdata.PrintToPrinter(1, false, 0, 0);
                }             
            }



        }

        private void dtpSaleDate_ValueChanged(object sender, EventArgs e)
        {
                    }

       
        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            if (btnAdd.Text == "Add")
            {

         
                InsertOrderDetails();
                SelectOrderDetails();
                CalculateSubTotal();
                Clear();
            }

            else
            {
                SetField();

                bl_sale.UpdateOrderDetails(bl_sale);

                //MessageBox.Show("Record Updated Successfully");
                SelectOrderDetails();
                CalculateSubTotal();
                Clear();
            }
        }

        private void txtInstal_TextChanged(object sender, EventArgs e)
        {

        }

        private void rdbVat_CheckedChanged(object sender, EventArgs e)
        {
            CalculateSubTotal();
        }

        private void unvatable_CheckedChanged(object sender, EventArgs e)
        {
            if (rdounvatable.Checked == true)
            {
                pnltax.Visible = false;
                
            }
            else {
                pnltax.Visible = true;
            }
        }

        private void txtCustomer_Leave(object sender, EventArgs e)
        {
            grdCustomer.Visible = false;
            if (txtCustomer.Text != "")
            {
                string table = common.ReturnOneValue("select FirstName From Customer where FirstName='" + txtCustomer.Text + "'");
                if (table != "")
                {

                }
                else
                {
                    MessageBox.Show("customer name not found?");
                    txtCustomer.Text = "";
                    txtCustomer.Focus();
                }
            }
        }

        private void txtProduct_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = null;


            if (txtProduct.Text != "")
            {

  


                bl_productname.TextSearch = txtProduct.Text;

                dt = bl_productname.SelectProductNameBySearch(bl_productname);


                if (dt.Rows.Count > 0)
                {
                    grdproduct.Visible = true;
                    grdproduct.DataSource = dt;
                    grdproduct.Columns["ProductNameId"].Visible = false;
                }

            }
            else
            {
                if (txtProduct.Focused)
                {
                    grdproduct.Visible = true;
                    dt = bl_productname.SelectProductName();
                    grdproduct.DataSource = dt;
                    grdproduct.Columns["ProductNameId"].Visible = false;
                    grdproduct.Columns["PurchaseQty"].Visible = false;
                    grdproduct.Columns["PurchaseRate"].Visible = false;
                    grdproduct.Columns["Reorderlevel"].Visible = false;
                  
                }


            }
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            CalculateItemAmount();
        }

        private void txtRate_TextChanged(object sender, EventArgs e)
        {
            CalculateItemAmount();
        }

        private void txtDiscount_TextChanged(object sender, EventArgs e)
        {
            CalculateItemAmount();
        }

        private void btncustomer_Click(object sender, EventArgs e)
        {
            Customer cm = new Customer();
            cm.ShowDialog();
        }


       

       

    }
}
