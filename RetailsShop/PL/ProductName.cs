﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;

namespace RetailsShop.PL
{

    public partial class ProductName : Form
    {


        BL_ProductName bl_productname = new BL_ProductName();
        BL_Manufacturer bl_manufacturer = new BL_Manufacturer();
        Common common = new Common();

        public ProductName()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void ProductName_Load(object sender, EventArgs e)
        {
            BindVendor();
            BindManufact();
          //  BindCompany();
        }

        //private void BindCompany()
        //{
        //    DataTable dt = bl_manufacturer.SelectCompany();
        //    if (dt.Rows.Count > 0)
        //    {
        //        ddlcompany.DataSource = dt;
        //        ddlcompany.DisplayMember = "CompanyName";
        //        ddlcompany.ValueMember = "TblId";

        //    }
        //    else
        //    {
        //        ddlproducttype.DataSource = null;
        //    }
        //}
        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnadd.Text == "Add")
                {
                    //if (txtProductName.Text == "")
                    //{
                    //    MessageBox.Show("Please enter Product Name");
                    //}
                    
                    //else if (txtPurchaseQty.Text=="")
                    //{
                    //    MessageBox.Show("Please enter Purchase Size");
                    //}
                    //else if (txtSaleRate.Text=="")
                    //{
                    //    MessageBox.Show("Please enter MRP");
                    //}
                    //else
                    //{
                        SetField();
                        bl_productname.InsertProductName(bl_productname);
                        MessageBox.Show("Record inserted Successfully");
                        BindVendor();
                        Clear();
                      
                    //}
                }
                //if(btnadd.Text == "Update")
                else
                {
                    SetField();
                    bl_productname.UpdateProductName(bl_productname);
                    MessageBox.Show("Record Updated Successfully");
                    BindVendor();
                    Clear();
                }
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");

            }
        }


        private void SetField()
        {
            bl_productname.ProductName = txtProductName.Text;
            bl_productname.ManufactureId = cmbManufactName.SelectedValue.ToString(); 
            bl_productname.PurchaseQty = txtPurchaseQty.Text;           
            bl_productname.PurchaseRate = txtPurchaseRate.Text;
            bl_productname.SaleRate = txtSaleRate.Text;
            bl_productname.Reorderlevel = txtReorderLevel.Text;
            
        }

        private void Clear()
        {

            txtProductName.Text="";
            cmbManufactName.Text = "";
            txtPurchaseQty.Text = "";
            txtPurchaseRate.Text = "";
            txtSaleRate.Text = "";         

            txtReorderLevel.Text = "";

            btnadd.Text = "Add";
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();
            btnadd.Text = "Add";
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_productname.ProductNameId = grdProductName.SelectedRows[0].Cells[0].Value.ToString();
                    bl_productname.DeleteProductName(bl_productname);
                   
                    MessageBox.Show("Record Delete Successfully");
                    BindVendor();
                    Clear();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_productname.ProductNameId = grdProductName.SelectedRows[0].Cells[0].Value.ToString();
            txtProductName.Text          = grdProductName.SelectedRows[0].Cells[1].Value.ToString();
            cmbManufactName.Text         = grdProductName.SelectedRows[0].Cells[2].Value.ToString();         
            txtPurchaseQty.Text          = grdProductName.SelectedRows[0].Cells[3].Value.ToString();          
            txtPurchaseRate.Text         = grdProductName.SelectedRows[0].Cells[4].Value.ToString();
            txtSaleRate.Text             = grdProductName.SelectedRows[0].Cells[5].Value.ToString();
            txtReorderLevel.Text         = grdProductName.SelectedRows[0].Cells[6].Value.ToString();
               
          
         

            btnadd.Text = "Update";
        }

        private void BindVendor()
        {
            //DataTable dt = bl_productname.SelectProductName();
            //grdProductName.DataSource = dt;
            //grdProductName.Columns["ProductNameId"].Visible = false;
            ////grdProductName.Columns["Status"].Visible = false;



            DataTable dt = bl_productname.SelectProductName();
            if (dt.Rows.Count > 0)
            {
                grdProductName.DataSource = dt;
                grdProductName.Columns["ProductNameId"].Visible = false;
                // grdProductName.Columns["Status"].Visible = false;
            }
            else
            {
                grdProductName.DataSource = null;
            }
        }

        private void BindManufact()
        {
            DataTable dt = bl_productname.SelectManufact();
            cmbManufactName.DataSource = dt;
            cmbManufactName.DisplayMember = "ManufactureName";
            cmbManufactName.ValueMember = "ManufactureId";


        }
        

       

        private void txtPurchaseRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.NumberOnly(e);
        }

            

        private void txtReorderLevel_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.NumberOnly(e);
        }

        private void txtPurchaseQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.NumberOnly(e);
        }

        private void txtSaleRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.NumberOnly(e);
        }

        private void cmbManufactName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }




    }
}
