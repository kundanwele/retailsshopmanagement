﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using RetailsShop.BL;
using RetailsShop.PL;
using RetailsShop.DL;

using System.Data.SqlClient;


namespace RetailsShop.PL
{
    public partial class CreateUser : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_CreateUser bl_createuser = new BL_CreateUser();


        public CreateUser()
        {
            InitializeComponent();
        }



        //========================Clear=============================



        private void Clear()
        {
            cmbSelUser.Text    = "";
            txtUserName.Text   = "";           
            txtPassword.Text   = "";
            txtRetypePass.Text = "";
            cmbUserRole.Text   = "";
        }



        //========================Form Load=============================



        private void BindData()
        {
            DataTable dt = bl_createuser.SelectCreateUser();
            dgvCreateUserInfo.DataSource = dt;

            dgvCreateUserInfo.Columns["CreateUserId"].Visible = false;
            //dgvCreateUserInfo.Columns["StaffId"].Visible    = false;  
            dgvCreateUserInfo.Columns["UserName"].Visible     = false;
            dgvCreateUserInfo.Columns["Password"].Visible     = false;
            dgvCreateUserInfo.Columns["Status"].Visible       = false;
            

        }




        //========================Form Load=============================




        private void CreateUser_Load(object sender, EventArgs e)
        {
           // BindData();
            //BindUser();


            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }




        //========================Set Field=============================




        private void SetField()
        {
            bl_field.WorkerId    = Convert.ToString(cmbSelUser.SelectedValue);
            bl_field.Username   = txtUserName.Text;
            bl_field.Password   = txtPassword.Text;
            bl_field.ReTypePass = txtRetypePass.Text;
            bl_field.Role   = cmbUserRole.Text;
        }



        //========================Save=============================




        private void btn_Save_Click(object sender, EventArgs e)
        {
            if(cmbSelUser.Text=="")
            {
                MessageBox.Show("Please Select User");
            }
            else if(txtUserName.Text=="")
            {
                MessageBox.Show("You Can't leave User Name empty ");
            }
            else if (txtPassword.Text == "")
            {
                MessageBox.Show("You Can't leave Password empty ");
            }
            else if (txtRetypePass.Text == "")
            {
                MessageBox.Show("You Can't leave Retype Password empty ");
               
            }
            else if (txtPassword.Text != txtRetypePass.Text)
            {
                MessageBox.Show("Your Retype Password must be same as Password ");

            }
            else if(cmbUserRole.Text=="")
            {
               MessageBox.Show("Please Select User Role ");
            }
            else
            {

               SetField();
               bl_createuser.InsertCreateUser(bl_field);

               BindData();
               MessageBox.Show("You are Successfully Registered");
               Clear();

               btn_Update.Enabled = false;
               btn_Delete.Enabled = false;
            }

        }



        //========================Cancel=============================



        private void btn_Cancel_Click(object sender, EventArgs e)
        {
          
                Clear();              
                   

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
           

        }


        //========================Bind User=============================



        private void BindUser()
        {
            DataTable dt = bl_createuser.SelectStaff(); // User Name coming from StaffRegistration
            cmbSelUser.DataSource = dt;

            cmbSelUser.DisplayMember = "FirstName";
            cmbSelUser.ValueMember = "StaffId";
        }



        //========================Grid View=============================


        private void dgvCreateUserInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvCreateUserInfo.Rows.Count > 0)
            {
                bl_field.WorkerId  = dgvCreateUserInfo.SelectedRows[0].Cells[0].Value.ToString();

                cmbSelUser.Text  = dgvCreateUserInfo.SelectedRows[0].Cells[1].Value.ToString();
                txtUserName.Text = dgvCreateUserInfo.SelectedRows[0].Cells[2].Value.ToString();
                txtPassword.Text = dgvCreateUserInfo.SelectedRows[0].Cells[3].Value.ToString();
                cmbUserRole.Text = dgvCreateUserInfo.SelectedRows[0].Cells[4].Value.ToString();

            }

            btn_Save.Enabled = false;
            btn_Update.Enabled = true;
            btn_Delete.Enabled = true;



        }



        //========================Update=============================



        private void btn_Update_Click(object sender, EventArgs e)
        {
            if(txtUserName.Text=="" || cmbUserRole.Text=="")
            {
                MessageBox.Show("Please Select Specific Record");
            }
            else
            {
               SetField();
               bl_createuser.UpdateCreateUser(bl_field);
               BindData();
               MessageBox.Show("Record Updated Successfully");
               Clear();
                }

        }



        //========================Delete=============================


        
        private void btn_Delete_Click(object sender, EventArgs e)
        {

            if (txtUserName.Text == "" || cmbUserRole.Text == "")
            {
                MessageBox.Show("Please Select Specific Record");
            }
            else
            {
                try
                {

                    DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Record?", "Delete", MessageBoxButtons.YesNo);
                    if (msgg == DialogResult.Yes)
                    {
                        bl_field.WorkerId = dgvCreateUserInfo.SelectedRows[0].Cells[0].Value.ToString();
                        bl_createuser.DeleteCreateUser(bl_field);
                        MessageBox.Show("Record Delete Successfully");
                        BindData();
                        Clear();
                    }
                }
                catch
                {

                    MessageBox.Show("error occured!Please contact Administration?");
                }
            }


        }

        private void CreateUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        
    }
}
