﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;

namespace RetailsShop.PL
{
    public partial class Manufacturer : Form
    {
        BL_Manufacturer bl_manufacture = new BL_Manufacturer();
        DL_Manufacturer dl_manufacture = new DL_Manufacturer();


        public Manufacturer()
        {
            InitializeComponent();
        }



        private void SetField()
        {

            bl_manufacture.ManufactureName = txtmanfName.Text;
            bl_manufacture.ManufactureDiscri = txtmanfDesc.Text;


        }


        private void Clear()
        {
            txtmanfName.Text = "";
            txtmanfDesc.Text = "";
        }

        private void BindManufac()
        {
            DataTable dt = bl_manufacture.SelectManufacturer();
            if (dt.Rows.Count > 0)
            {
                dgvManufacture.DataSource = dt;
                dgvManufacture.Columns["ManufactureId"].Visible = false;
                dgvManufacture.Columns["Status"].Visible = false;
            }
            else
            {
                dgvManufacture.DataSource = null;
            }
        }


        private void Manufacturer_Load(object sender, EventArgs e)
        {
            BindManufac();

            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }

      

       
        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (txtmanfName.Text == "")
            {
                MessageBox.Show("Please enter Manufacturer Name ");
            }
            else if (txtmanfDesc.Text == "")
            {
                MessageBox.Show("Please enter Manufacturer Description");
            }
            else
            {

                SetField();
                bl_manufacture.InsertManufacturer(bl_manufacture);

                Clear();
                MessageBox.Show("Record inserted Successfully");
                BindManufac();

            }

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }



        private void btn_Update_Click(object sender, EventArgs e)
        {
            SetField();
            bl_manufacture.UpdateManufacturer(bl_manufacture);

            Clear();
            MessageBox.Show("Record Updated Successfully");
            BindManufac();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;

        }








        private void btn_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {

                    bl_manufacture.ManufactureId = dgvManufacture.SelectedRows[0].Cells[0].Value.ToString();
                    SetField();

                    bl_manufacture.DeleteManufacturer(bl_manufacture);
                   

                    Clear();
                    MessageBox.Show("Record Delete Successfully");
                    BindManufac();

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }


            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;

        }








        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Clear();


            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }






        private void dgvManufacture_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
                bl_manufacture.ManufactureId = dgvManufacture.SelectedRows[0].Cells[0].Value.ToString();
                
                txtmanfName.Text = dgvManufacture.SelectedRows[0].Cells[1].Value.ToString();
                txtmanfDesc.Text = dgvManufacture.SelectedRows[0].Cells[2].Value.ToString();
            

            btn_Save.Enabled = false;
            btn_Update.Enabled = true;
            btn_Delete.Enabled = true;

        }

        private void dgvManufacture_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtmanfName_TextChanged(object sender, EventArgs e)
        {

        }







    }
}
