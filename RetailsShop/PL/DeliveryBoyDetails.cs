﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;


namespace RetailsShop.PL
{
    public partial class DeliveryBoyDetails : Form
    {
        DL_DeliveryBoy dl_deliveryboy = new DL_DeliveryBoy();
        BL_DeliveryBoy bl_deliveryboy = new BL_DeliveryBoy();


        public DeliveryBoyDetails()
        {
            InitializeComponent();
        }



        private void SetField()
        {
            bl_deliveryboy.Name       = txtname.Text;
            bl_deliveryboy.MiddleName = txtmiddlename.Text;
            bl_deliveryboy.LastName   = txtlastname.Text;
            bl_deliveryboy.PhoneNo    = txtphone.Text;
            bl_deliveryboy.Mobile     = txtmobileno.Text;
            bl_deliveryboy.Email      = txtemail.Text;
            bl_deliveryboy.Country    = txtcountry.Text;
            bl_deliveryboy.State      = txtstate.Text;
            bl_deliveryboy.City       = txtcity.Text;
            bl_deliveryboy.Address    = txtaddress.Text;


        }


        private void ClearField()
        {
            txtname.Text = "";
            txtmiddlename.Text = "";
            txtlastname.Text = "";
            txtphone.Text = "";
            txtmobileno.Text = "";
            txtemail.Text = "";
            txtcountry.Text = "";
            txtstate.Text = "";
            txtcity.Text = "";
            txtaddress.Text = "";

        }


        private void BindDeliveryBoy()
        {
            DataTable dt = bl_deliveryboy.SelectDeliveryBoy();
           grdDeliveryBoys.DataSource = dt;
           grdDeliveryBoys.Columns["DeliveryBoysId"].Visible = false;
           grdDeliveryBoys.Columns["Status"].Visible = false;
        }


        
        private void DeliveryBoyDetails_Load(object sender, EventArgs e)
        {
            BindDeliveryBoy();

            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }


        private void btn_Save_Click(object sender, EventArgs e)
        {
            SetField();
            bl_deliveryboy.InsertDeliveryBoy(bl_deliveryboy);
            MessageBox.Show("Record Inserted successfully");
            ClearField();
            BindDeliveryBoy();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;

        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            SetField();
            bl_deliveryboy.UpdateDeliveryBoy(bl_deliveryboy);
            MessageBox.Show("Record Updated successfully");
            ClearField();
            BindDeliveryBoy();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }



        private void btn_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_deliveryboy.DeleteDeliveryBoy(bl_deliveryboy);
                    ClearField();
                    MessageBox.Show("Record Deleted Successfully");
                    BindDeliveryBoy();

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }



        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            ClearField();

            btn_Save.Enabled = true;
            btn_Update.Enabled = false;
            btn_Delete.Enabled = false;
        }


        private void grdDeliveryBoys_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void txtphone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;

            }
        }

        private void txtmobileno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;

            }
        }

        private void DeliveryBoyDetails_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

        }

        private void grdDeliveryBoys_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                bl_deliveryboy.DeliveryBoysId = grdDeliveryBoys.SelectedRows[0].Cells[0].Value.ToString();
                txtname.Text = grdDeliveryBoys.SelectedRows[0].Cells[1].Value.ToString();
                txtmiddlename.Text = grdDeliveryBoys.SelectedRows[0].Cells[2].Value.ToString();
                txtlastname.Text = grdDeliveryBoys.SelectedRows[0].Cells[3].Value.ToString();
                txtphone.Text = grdDeliveryBoys.SelectedRows[0].Cells[4].Value.ToString();
                txtmobileno.Text = grdDeliveryBoys.SelectedRows[0].Cells[5].Value.ToString();
                txtemail.Text = grdDeliveryBoys.SelectedRows[0].Cells[6].Value.ToString();
                txtcountry.Text = grdDeliveryBoys.SelectedRows[0].Cells[7].Value.ToString();
                txtstate.Text = grdDeliveryBoys.SelectedRows[0].Cells[8].Value.ToString();
                txtcity.Text = grdDeliveryBoys.SelectedRows[0].Cells[9].Value.ToString();
                txtaddress.Text = grdDeliveryBoys.SelectedRows[0].Cells[10].Value.ToString();

            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }


            btn_Save.Enabled = false;
            btn_Update.Enabled = true;
            btn_Delete.Enabled = true;

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
