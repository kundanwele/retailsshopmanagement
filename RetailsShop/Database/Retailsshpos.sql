/****** Object:  Default [DF_TreeViewItem_Status]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TreeViewItem_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[TreeViewItem]'))
Begin
ALTER TABLE [dbo].[TreeViewItem] DROP CONSTRAINT [DF_TreeViewItem_Status]

End
GO
/****** Object:  Table [dbo].[SmsSetting]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SmsSetting]') AND type in (N'U'))
DROP TABLE [dbo].[SmsSetting]
GO
/****** Object:  Table [dbo].[OrganisationDetails]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganisationDetails]') AND type in (N'U'))
DROP TABLE [dbo].[OrganisationDetails]
GO
/****** Object:  Table [dbo].[OtherExpences]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OtherExpences]') AND type in (N'U'))
DROP TABLE [dbo].[OtherExpences]
GO
/****** Object:  Table [dbo].[AddProduct]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddProduct]') AND type in (N'U'))
DROP TABLE [dbo].[AddProduct]
GO
/****** Object:  Table [dbo].[PurchaseDetail]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurchaseDetail]') AND type in (N'U'))
DROP TABLE [dbo].[PurchaseDetail]
GO
/****** Object:  Table [dbo].[Purchase]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Purchase]') AND type in (N'U'))
DROP TABLE [dbo].[Purchase]
GO
/****** Object:  Table [dbo].[Stock]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Stock]') AND type in (N'U'))
DROP TABLE [dbo].[Stock]
GO
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrderDetails]') AND type in (N'U'))
DROP TABLE [dbo].[OrderDetails]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Orders]') AND type in (N'U'))
DROP TABLE [dbo].[Orders]
GO
/****** Object:  Table [dbo].[Insatllment]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Insatllment]') AND type in (N'U'))
DROP TABLE [dbo].[Insatllment]
GO
/****** Object:  Table [dbo].[InstallmentDeatails]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InstallmentDeatails]') AND type in (N'U'))
DROP TABLE [dbo].[InstallmentDeatails]
GO
/****** Object:  Table [dbo].[Manufacturer]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Manufacturer]') AND type in (N'U'))
DROP TABLE [dbo].[Manufacturer]
GO
/****** Object:  Table [dbo].[Destributors]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Destributors]') AND type in (N'U'))
DROP TABLE [dbo].[Destributors]
GO
/****** Object:  Table [dbo].[ManagerDetails]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ManagerDetails]') AND type in (N'U'))
DROP TABLE [dbo].[ManagerDetails]
GO
/****** Object:  Table [dbo].[DeliveryBoys]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeliveryBoys]') AND type in (N'U'))
DROP TABLE [dbo].[DeliveryBoys]
GO
/****** Object:  Table [dbo].[CreateUser]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateUser]') AND type in (N'U'))
DROP TABLE [dbo].[CreateUser]
GO
/****** Object:  Table [dbo].[PaymentMode]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PaymentMode]') AND type in (N'U'))
DROP TABLE [dbo].[PaymentMode]
GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Supplier]') AND type in (N'U'))
DROP TABLE [dbo].[Supplier]
GO
/****** Object:  Table [dbo].[Tax]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tax]') AND type in (N'U'))
DROP TABLE [dbo].[Tax]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer]') AND type in (N'U'))
DROP TABLE [dbo].[Customer]
GO
/****** Object:  Table [dbo].[TreeViewMenu]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TreeViewMenu]') AND type in (N'U'))
DROP TABLE [dbo].[TreeViewMenu]
GO
/****** Object:  Table [dbo].[TreeViewItem]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TreeViewItem]') AND type in (N'U'))
DROP TABLE [dbo].[TreeViewItem]
GO
/****** Object:  Table [dbo].[PrinterSetup]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PrinterSetup]') AND type in (N'U'))
DROP TABLE [dbo].[PrinterSetup]
GO
/****** Object:  Table [dbo].[EmailSetting]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmailSetting]') AND type in (N'U'))
DROP TABLE [dbo].[EmailSetting]
GO
/****** Object:  Table [dbo].[Worker]    Script Date: 04/22/2015 18:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Worker]') AND type in (N'U'))
DROP TABLE [dbo].[Worker]
GO
/****** Object:  Table [dbo].[Worker]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Worker]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Worker](
	[WorkerId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MiddleName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Mobile] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Role] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Username] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Password] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Worker] PRIMARY KEY CLUSTERED 
(
	[WorkerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Worker] ON
INSERT [dbo].[Worker] ([WorkerId], [Name], [MiddleName], [LastName], [PhoneNo], [Mobile], [Email], [Country], [State], [City], [Address], [Role], [Username], [Password]) VALUES (1, N'ssssssss', N'cccccccccc', N'sssssssc', N'555555555', N'8888', N'fkjk', N'dksjfkj', N'fndnf', N'ngb', N'vfndndj', N'Admin', N'kundan', N'')
SET IDENTITY_INSERT [dbo].[Worker] OFF
/****** Object:  Table [dbo].[EmailSetting]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmailSetting]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmailSetting](
	[EmailId] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Password] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Host] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Port] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_EmailSetting] PRIMARY KEY CLUSTERED 
(
	[EmailId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[EmailSetting] ON
INSERT [dbo].[EmailSetting] ([EmailId], [Email], [Password], [Host], [Port]) VALUES (2, N'xzzxzx', N'ddddddddddddddd', N'dddddddddddddd', N'dddddddddddd')
SET IDENTITY_INSERT [dbo].[EmailSetting] OFF
/****** Object:  Table [dbo].[PrinterSetup]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PrinterSetup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PrinterSetup](
	[PrintId] [int] IDENTITY(1,1) NOT NULL,
	[BillPrinter] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[KotPrinter] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PrinterSetup] PRIMARY KEY CLUSTERED 
(
	[PrintId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[PrinterSetup] ON
INSERT [dbo].[PrinterSetup] ([PrintId], [BillPrinter], [KotPrinter], [Status]) VALUES (1, N'Microsoft XPS Document Writer', N'Microsoft XPS Document Writer', NULL)
INSERT [dbo].[PrinterSetup] ([PrintId], [BillPrinter], [KotPrinter], [Status]) VALUES (2, N'Fax', N'Microsoft XPS Document Writer', NULL)
INSERT [dbo].[PrinterSetup] ([PrintId], [BillPrinter], [KotPrinter], [Status]) VALUES (3, N'Fax', N'Fax', NULL)
SET IDENTITY_INSERT [dbo].[PrinterSetup] OFF
/****** Object:  Table [dbo].[TreeViewItem]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TreeViewItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TreeViewItem](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[MenuId] [int] NULL,
	[ItemName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_TreeViewItem] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[TreeViewItem] ON
INSERT [dbo].[TreeViewItem] ([ItemId], [MenuId], [ItemName], [Status]) VALUES (2, 1, N'bbhb', 1)
INSERT [dbo].[TreeViewItem] ([ItemId], [MenuId], [ItemName], [Status]) VALUES (4, 7, N'fcdfd', 1)
INSERT [dbo].[TreeViewItem] ([ItemId], [MenuId], [ItemName], [Status]) VALUES (5, 6, N'vffdscd', 1)
INSERT [dbo].[TreeViewItem] ([ItemId], [MenuId], [ItemName], [Status]) VALUES (8, 11, N'c', 1)
INSERT [dbo].[TreeViewItem] ([ItemId], [MenuId], [ItemName], [Status]) VALUES (9, 12, N'fdf', 1)
SET IDENTITY_INSERT [dbo].[TreeViewItem] OFF
/****** Object:  Table [dbo].[TreeViewMenu]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TreeViewMenu]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TreeViewMenu](
	[MenuId] [int] IDENTITY(1,1) NOT NULL,
	[MenuName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sequance] [int] NULL,
 CONSTRAINT [PK_TreeViewMenu] PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[TreeViewMenu] ON
INSERT [dbo].[TreeViewMenu] ([MenuId], [MenuName], [Sequance]) VALUES (11, N'cccccccccc', NULL)
INSERT [dbo].[TreeViewMenu] ([MenuId], [MenuName], [Sequance]) VALUES (12, N'vvvvvvvvvvvv', NULL)
INSERT [dbo].[TreeViewMenu] ([MenuId], [MenuName], [Sequance]) VALUES (14, N'bbbbb', NULL)
SET IDENTITY_INSERT [dbo].[TreeViewMenu] OFF
/****** Object:  Table [dbo].[Customer]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MiddleName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Mobile] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Customer] ON
INSERT [dbo].[Customer] ([CustomerId], [FirstName], [MiddleName], [LastName], [PhoneNo], [Mobile], [Email], [Country], [State], [City], [Address], [Status], [Type]) VALUES (4, N'x', N'a', N'w', N'3', N'5', N'f', N'c', N'n', N'm', N'k', NULL, NULL)
INSERT [dbo].[Customer] ([CustomerId], [FirstName], [MiddleName], [LastName], [PhoneNo], [Mobile], [Email], [Country], [State], [City], [Address], [Status], [Type]) VALUES (5, N'sapna', N'p', N'k', N'442423', N'233332', N'', N'', N'', N'', N'', NULL, 1)
INSERT [dbo].[Customer] ([CustomerId], [FirstName], [MiddleName], [LastName], [PhoneNo], [Mobile], [Email], [Country], [State], [City], [Address], [Status], [Type]) VALUES (8, N'sa', N'a', N'', N'', N'', N'', N'', N'', N'', N'', NULL, 1)
INSERT [dbo].[Customer] ([CustomerId], [FirstName], [MiddleName], [LastName], [PhoneNo], [Mobile], [Email], [Country], [State], [City], [Address], [Status], [Type]) VALUES (9, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, 1)
INSERT [dbo].[Customer] ([CustomerId], [FirstName], [MiddleName], [LastName], [PhoneNo], [Mobile], [Email], [Country], [State], [City], [Address], [Status], [Type]) VALUES (10, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, 1)
INSERT [dbo].[Customer] ([CustomerId], [FirstName], [MiddleName], [LastName], [PhoneNo], [Mobile], [Email], [Country], [State], [City], [Address], [Status], [Type]) VALUES (11, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, 0)
SET IDENTITY_INSERT [dbo].[Customer] OFF
/****** Object:  Table [dbo].[Tax]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tax]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Tax](
	[TaxId] [int] IDENTITY(1,1) NOT NULL,
	[TaxRate] [int] NULL,
	[TaxDescription] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Tax] PRIMARY KEY CLUSTERED 
(
	[TaxId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Tax] ON
INSERT [dbo].[Tax] ([TaxId], [TaxRate], [TaxDescription], [Status]) VALUES (6, 10, N'fdfsf', NULL)
SET IDENTITY_INSERT [dbo].[Tax] OFF
/****** Object:  Table [dbo].[Supplier]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Supplier]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Supplier](
	[SupplierId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ContactPerson] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MobileNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Note] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Vendor] PRIMARY KEY CLUSTERED 
(
	[SupplierId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Supplier] ON
INSERT [dbo].[Supplier] ([SupplierId], [SupplierName], [ContactPerson], [Country], [State], [City], [Address], [PhoneNo], [MobileNo], [Email], [Note], [Status]) VALUES (3, N'sdfdsf', N'', N'', N'', N'', N'', N'6546', N'', N'', N'', NULL)
INSERT [dbo].[Supplier] ([SupplierId], [SupplierName], [ContactPerson], [Country], [State], [City], [Address], [PhoneNo], [MobileNo], [Email], [Note], [Status]) VALUES (5, N'xvxv', N'', N'', N'SXzX', N'', N'', N'', N'', N'', N'czdcdc', NULL)
SET IDENTITY_INSERT [dbo].[Supplier] OFF
/****** Object:  Table [dbo].[PaymentMode]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PaymentMode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PaymentMode](
	[PaymentModeId] [int] IDENTITY(1,1) NOT NULL,
	[PaymentMode] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PaymentMode] PRIMARY KEY CLUSTERED 
(
	[PaymentModeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[PaymentMode] ON
INSERT [dbo].[PaymentMode] ([PaymentModeId], [PaymentMode]) VALUES (1, N'ggxczc')
INSERT [dbo].[PaymentMode] ([PaymentModeId], [PaymentMode]) VALUES (6, N'XXXXX')
SET IDENTITY_INSERT [dbo].[PaymentMode] OFF
/****** Object:  Table [dbo].[CreateUser]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CreateUser](
	[CreateUserId] [int] IDENTITY(1,1) NOT NULL,
	[StaffId] [int] NULL,
	[UserName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Password] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UserRole] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_CreateUser] PRIMARY KEY CLUSTERED 
(
	[CreateUserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[CreateUser] ON
INSERT [dbo].[CreateUser] ([CreateUserId], [StaffId], [UserName], [Password], [UserRole], [Status]) VALUES (1, 1, N'kundan', N'kk', N'Admin', N'Active    ')
SET IDENTITY_INSERT [dbo].[CreateUser] OFF
/****** Object:  Table [dbo].[DeliveryBoys]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeliveryBoys]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DeliveryBoys](
	[DeliveryBoysId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MiddleName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Mobile] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_DeliveryBoys] PRIMARY KEY CLUSTERED 
(
	[DeliveryBoysId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[DeliveryBoys] ON
INSERT [dbo].[DeliveryBoys] ([DeliveryBoysId], [Name], [MiddleName], [LastName], [PhoneNo], [Mobile], [Email], [Country], [State], [City], [Address], [Status]) VALUES (1, N'dad', N'das', N'sdsa', N'654', N'2321', N'efsf', N'dcdzc', N'dcs', N'czxc', N'cx', NULL)
SET IDENTITY_INSERT [dbo].[DeliveryBoys] OFF
/****** Object:  Table [dbo].[ManagerDetails]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ManagerDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ManagerDetails](
	[ManagerId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MiddleName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Mobile] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_ManagerDetails] PRIMARY KEY CLUSTERED 
(
	[ManagerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[ManagerDetails] ON
INSERT [dbo].[ManagerDetails] ([ManagerId], [Name], [MiddleName], [LastName], [PhoneNo], [Mobile], [Email], [Country], [State], [City], [Address], [Status]) VALUES (12, N'sdd', N'sD', N'', N'', N'4444444', N'', N'', N'', N'', N'', NULL)
SET IDENTITY_INSERT [dbo].[ManagerDetails] OFF
/****** Object:  Table [dbo].[Destributors]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Destributors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Destributors](
	[DistributorsId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MiddleName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Mobile] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Destributors] PRIMARY KEY CLUSTERED 
(
	[DistributorsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Destributors] ON
INSERT [dbo].[Destributors] ([DistributorsId], [Name], [MiddleName], [LastName], [PhoneNo], [Mobile], [Email], [Country], [State], [City], [Address], [Status]) VALUES (7, N'czczc', N'', N'dfd', N'646', N'', N'', N'', N'', N'', N'', NULL)
INSERT [dbo].[Destributors] ([DistributorsId], [Name], [MiddleName], [LastName], [PhoneNo], [Mobile], [Email], [Country], [State], [City], [Address], [Status]) VALUES (9, N'trgrdgvc', N'', N' dfdf', N'', N'', N'', N'', N'', N'', N'6yytyh', NULL)
SET IDENTITY_INSERT [dbo].[Destributors] OFF
/****** Object:  Table [dbo].[Manufacturer]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Manufacturer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Manufacturer](
	[ManufactureId] [int] IDENTITY(1,1) NOT NULL,
	[ManufactureName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ManufactureDiscri] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Manufacturer] PRIMARY KEY CLUSTERED 
(
	[ManufactureId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Manufacturer] ON
INSERT [dbo].[Manufacturer] ([ManufactureId], [ManufactureName], [ManufactureDiscri], [Status]) VALUES (2, N'dsdfrswwwwwwwwwwwww', N'erefdsf', NULL)
INSERT [dbo].[Manufacturer] ([ManufactureId], [ManufactureName], [ManufactureDiscri], [Status]) VALUES (4, N'aaaaaaaaaaa', N'ww', NULL)
INSERT [dbo].[Manufacturer] ([ManufactureId], [ManufactureName], [ManufactureDiscri], [Status]) VALUES (5, N'sas', N'aaaaaa', NULL)
SET IDENTITY_INSERT [dbo].[Manufacturer] OFF
/****** Object:  Table [dbo].[InstallmentDeatails]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InstallmentDeatails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[InstallmentDeatails](
	[InstDetailId] [int] IDENTITY(1,1) NOT NULL,
	[InstId] [int] NULL,
	[Amount] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PayDate] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_InstallmentDeatails] PRIMARY KEY CLUSTERED 
(
	[InstDetailId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Insatllment]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Insatllment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Insatllment](
	[InstId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NULL,
	[NoOfInst] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Amount] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PayDate] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NextpayDate] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Insatllment] PRIMARY KEY CLUSTERED 
(
	[InstId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Orders]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Orders](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[Date] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InvoiceNo] [int] NULL,
	[Vat] [int] NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Orders] ON
INSERT [dbo].[Orders] ([OrderId], [CustomerId], [Date], [InvoiceNo], [Vat]) VALUES (16, 4, N'18/04/2015', 0, 10)
INSERT [dbo].[Orders] ([OrderId], [CustomerId], [Date], [InvoiceNo], [Vat]) VALUES (17, NULL, N'12/3/2015', 100, 10)
INSERT [dbo].[Orders] ([OrderId], [CustomerId], [Date], [InvoiceNo], [Vat]) VALUES (18, NULL, N'12/3/2015', 100, 10)
INSERT [dbo].[Orders] ([OrderId], [CustomerId], [Date], [InvoiceNo], [Vat]) VALUES (19, 1, N'10/2/2015', 100, 10)
INSERT [dbo].[Orders] ([OrderId], [CustomerId], [Date], [InvoiceNo], [Vat]) VALUES (20, 1, N'10/2/2015', 100, 10)
INSERT [dbo].[Orders] ([OrderId], [CustomerId], [Date], [InvoiceNo], [Vat]) VALUES (21, 4, N'21/04/2015', 100, 10)
INSERT [dbo].[Orders] ([OrderId], [CustomerId], [Date], [InvoiceNo], [Vat]) VALUES (22, 4, N'21/04/2015', 100, 10)
SET IDENTITY_INSERT [dbo].[Orders] OFF
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrderDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OrderDetails](
	[OrderDetailId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NULL,
	[ProductId] [int] NULL,
	[SerialNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rate] [decimal](18, 2) NULL,
	[Qty] [int] NULL,
	[Discount] [int] NULL,
 CONSTRAINT [PK_OrderDetails] PRIMARY KEY CLUSTERED 
(
	[OrderDetailId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[OrderDetails] ON
INSERT [dbo].[OrderDetails] ([OrderDetailId], [OrderId], [ProductId], [SerialNo], [Rate], [Qty], [Discount]) VALUES (78, 0, 1, N'11111111111', CAST(10.00 AS Decimal(18, 2)), 2, 10)
INSERT [dbo].[OrderDetails] ([OrderDetailId], [OrderId], [ProductId], [SerialNo], [Rate], [Qty], [Discount]) VALUES (79, 0, 1, N'2222', CAST(10.00 AS Decimal(18, 2)), 1, 0)
INSERT [dbo].[OrderDetails] ([OrderDetailId], [OrderId], [ProductId], [SerialNo], [Rate], [Qty], [Discount]) VALUES (81, 0, 1, N'22', CAST(33.00 AS Decimal(18, 2)), 3, 0)
INSERT [dbo].[OrderDetails] ([OrderDetailId], [OrderId], [ProductId], [SerialNo], [Rate], [Qty], [Discount]) VALUES (84, 0, 11111, N'2', CAST(22.00 AS Decimal(18, 2)), 2, 0)
INSERT [dbo].[OrderDetails] ([OrderDetailId], [OrderId], [ProductId], [SerialNo], [Rate], [Qty], [Discount]) VALUES (85, 0, 11, N'2', CAST(22.00 AS Decimal(18, 2)), 1, 0)
INSERT [dbo].[OrderDetails] ([OrderDetailId], [OrderId], [ProductId], [SerialNo], [Rate], [Qty], [Discount]) VALUES (86, 0, 1, N'2222', CAST(10.00 AS Decimal(18, 2)), 1, 0)
INSERT [dbo].[OrderDetails] ([OrderDetailId], [OrderId], [ProductId], [SerialNo], [Rate], [Qty], [Discount]) VALUES (87, 0, 3, N'4444', CAST(30.00 AS Decimal(18, 2)), 1, 0)
INSERT [dbo].[OrderDetails] ([OrderDetailId], [OrderId], [ProductId], [SerialNo], [Rate], [Qty], [Discount]) VALUES (88, 0, 2222, N'2222222', CAST(30.00 AS Decimal(18, 2)), 1, 0)
INSERT [dbo].[OrderDetails] ([OrderDetailId], [OrderId], [ProductId], [SerialNo], [Rate], [Qty], [Discount]) VALUES (89, 0, 444, N'1', CAST(2.00 AS Decimal(18, 2)), 1, 0)
SET IDENTITY_INSERT [dbo].[OrderDetails] OFF
/****** Object:  Table [dbo].[Stock]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Stock]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Stock](
	[StockId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[ManufactureId] [int] NULL,
	[QtyInHand] [int] NULL,
 CONSTRAINT [PK_Stock] PRIMARY KEY CLUSTERED 
(
	[StockId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Purchase]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Purchase]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Purchase](
	[PurchaseId] [int] IDENTITY(1,1) NOT NULL,
	[ManufactureId] [int] NULL,
	[SupplierId] [int] NULL,
	[Date] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InvoiceNo] [int] NULL,
	[Vat] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Purchase] PRIMARY KEY CLUSTERED 
(
	[PurchaseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Purchase] ON
INSERT [dbo].[Purchase] ([PurchaseId], [ManufactureId], [SupplierId], [Date], [InvoiceNo], [Vat]) VALUES (1, 4, 5, N'22/04/2015', 44345355, N'')
INSERT [dbo].[Purchase] ([PurchaseId], [ManufactureId], [SupplierId], [Date], [InvoiceNo], [Vat]) VALUES (2, 5, 5, N'22/04/2015', 2122122, N'10')
INSERT [dbo].[Purchase] ([PurchaseId], [ManufactureId], [SupplierId], [Date], [InvoiceNo], [Vat]) VALUES (3, 5, 5, N'22/04/2015', 2122122, N'10')
INSERT [dbo].[Purchase] ([PurchaseId], [ManufactureId], [SupplierId], [Date], [InvoiceNo], [Vat]) VALUES (4, 5, 5, N'22/04/2015', 2122122, N'10')
SET IDENTITY_INSERT [dbo].[Purchase] OFF
/****** Object:  Table [dbo].[PurchaseDetail]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurchaseDetail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PurchaseDetail](
	[PurchaseDetailId] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseId] [int] NULL,
	[ProductNameId] [int] NULL,
	[SerialNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Qty] [int] NULL,
	[Rate] [decimal](18, 2) NULL,
	[Discount] [int] NULL,
	[Status] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PurchaseDetail] PRIMARY KEY CLUSTERED 
(
	[PurchaseDetailId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[PurchaseDetail] ON
INSERT [dbo].[PurchaseDetail] ([PurchaseDetailId], [PurchaseId], [ProductNameId], [SerialNo], [Qty], [Rate], [Discount], [Status]) VALUES (25, 0, 22, N'3', 1, CAST(3.00 AS Decimal(18, 2)), 0, N'')
INSERT [dbo].[PurchaseDetail] ([PurchaseDetailId], [PurchaseId], [ProductNameId], [SerialNo], [Qty], [Rate], [Discount], [Status]) VALUES (26, 0, 333, N'3', 1, CAST(22.00 AS Decimal(18, 2)), 0, N'')
INSERT [dbo].[PurchaseDetail] ([PurchaseDetailId], [PurchaseId], [ProductNameId], [SerialNo], [Qty], [Rate], [Discount], [Status]) VALUES (27, 0, 3232, N'2222', 1, CAST(3.00 AS Decimal(18, 2)), 0, N'')
INSERT [dbo].[PurchaseDetail] ([PurchaseDetailId], [PurchaseId], [ProductNameId], [SerialNo], [Qty], [Rate], [Discount], [Status]) VALUES (28, 0, 3333, N'22', 1, CAST(222.00 AS Decimal(18, 2)), 0, N'')
INSERT [dbo].[PurchaseDetail] ([PurchaseDetailId], [PurchaseId], [ProductNameId], [SerialNo], [Qty], [Rate], [Discount], [Status]) VALUES (29, 0, 8, N'8', 6, CAST(5.00 AS Decimal(18, 2)), 0, N'')
INSERT [dbo].[PurchaseDetail] ([PurchaseDetailId], [PurchaseId], [ProductNameId], [SerialNo], [Qty], [Rate], [Discount], [Status]) VALUES (30, 0, 53, N'3', 1, CAST(3.00 AS Decimal(18, 2)), 0, N'')
SET IDENTITY_INSERT [dbo].[PurchaseDetail] OFF
/****** Object:  Table [dbo].[AddProduct]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddProduct]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AddProduct](
	[ProductNameId] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ManufactureId] [int] NULL,
	[PurchaseQty] [int] NULL,
	[PurchaseRate] [decimal](18, 2) NULL,
	[SaleRate] [decimal](18, 2) NULL,
	[Reorderlevel] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_AddProduct] PRIMARY KEY CLUSTERED 
(
	[ProductNameId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[AddProduct] ON
INSERT [dbo].[AddProduct] ([ProductNameId], [ProductName], [ManufactureId], [PurchaseQty], [PurchaseRate], [SaleRate], [Reorderlevel], [Status]) VALUES (1, N'dsdbnbb', 4, 3, CAST(22.00 AS Decimal(18, 2)), CAST(22.00 AS Decimal(18, 2)), N'22', NULL)
SET IDENTITY_INSERT [dbo].[AddProduct] OFF
/****** Object:  Table [dbo].[OtherExpences]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OtherExpences]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OtherExpences](
	[ExpensesId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SessionId] [int] NULL,
	[ExpensesName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Amount] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Remark] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_OtherExpenses] PRIMARY KEY CLUSTERED 
(
	[ExpensesId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[OrganisationDetails]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganisationDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OrganisationDetails](
	[OrgId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneNo] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OfficeNo] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_OrganisationDetails] PRIMARY KEY CLUSTERED 
(
	[OrgId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[OrganisationDetails] ON
INSERT [dbo].[OrganisationDetails] ([OrgId], [Name], [Address], [PhoneNo], [OfficeNo]) VALUES (1, N'q', N'q', N'q', N'qq')
SET IDENTITY_INSERT [dbo].[OrganisationDetails] OFF
/****** Object:  Table [dbo].[SmsSetting]    Script Date: 04/22/2015 18:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SmsSetting]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SmsSetting](
	[SmsId] [int] IDENTITY(1,1) NOT NULL,
	[Url] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UserId] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Password] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_SmsSetting] PRIMARY KEY CLUSTERED 
(
	[SmsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[SmsSetting] ON
INSERT [dbo].[SmsSetting] ([SmsId], [Url], [UserId], [Password]) VALUES (6, N'xxxxxxxxxxxxx', N'wwwww', N'm')
SET IDENTITY_INSERT [dbo].[SmsSetting] OFF
/****** Object:  Default [DF_TreeViewItem_Status]    Script Date: 04/22/2015 18:03:29 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TreeViewItem_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[TreeViewItem]'))
Begin
ALTER TABLE [dbo].[TreeViewItem] ADD  CONSTRAINT [DF_TreeViewItem_Status]  DEFAULT ((1)) FOR [Status]

End
GO
