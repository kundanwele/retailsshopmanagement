﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;

using System.Configuration;



using System.Windows.Forms;

namespace RetailsShop.DL

{
    
    class DL_Connection
    {

        public static string constr = ConfigurationManager.ConnectionStrings["MuktanganConnectionString"].ConnectionString;

// public static string constr = @"data source=KPALMS\SQLEXPRESS;initial catalog=Muktangan;integrated security=true;";
//public static string constr = @"data source=.\SQLEXPRESS;AttachDbFilename=" + Application.StartupPath + "\\db\\Muktangan.mdf;integrated security=true;Connect Timeout=30;User Instance=true;";

// public static string constr = @"data source=.\SQLEXPRESS;AttachDbFilename=" + Application.StartupPath + "\\db\\Muktangan.mdf;integrated security=true;Connect Timeout=30;";

//public static string constr = @"Data Source=KPALMS;Initial Catalog=Muktangan;;Persist Security Info=True;User ID=sa;Password=kk;User Instance=False;Context Connection=False";


         SqlConnection con = new SqlConnection(constr);
        private void CheckCon()
        {
            try
            {
                if (con.State.ToString() == con.State.ToString())
                {
                    con.Close();
                }
            }
            catch 
            {
                
             
            }
        }

        public void UseExcuteNonQuery(string query)
        {
            try
            {
                CheckCon();
                con.Open();
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch 
            {
                
                
            }

        }

        public DataTable UseDatatable(string str)
        {
            
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand(str, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
      

        }
        public DataSet UseDataSet(string str)
        {
            DataSet dt = new DataSet();
            SqlCommand cmd = new SqlCommand(str, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }
        //public string  UseExcuteScaler(string str)
        //{

        //    CheckCon();
        //    con.Open();
        //    String id = "";
        //    SqlCommand cmd = new SqlCommand(str, con);
        //   Object tbl =cmd .ExecuteScalar ( );
        //   if (tbl != null)
        //   {
        //       id = tbl.ToString();
        //   }
        //   return id ;
           

        //}

        public string UseExcuteScaler(string str)
        {


            String closing = "";
            try
            {
                CheckCon();
                con.Open();
                SqlCommand cmd = new SqlCommand(str, con);
                Object tbl = cmd.ExecuteScalar();
                if (tbl != null)
                {
                    closing = tbl.ToString();
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Error Occured?Please Contact Administrator" + ex);
            }

            return closing;
        }

        public SqlDataReader UseExcuteDataReader(string str)
        {
            SqlDataReader dr=null ;
            try
            {
                CheckCon();
                con.Open();
                SqlCommand cmd = new SqlCommand(str, con);
                 dr= cmd.ExecuteReader(CommandBehavior.CloseConnection);
               
         
            }
            catch 
            {
                if (con != null)
                {
                    con.Close();
                }
                
            }
            return dr;

        }



        public void UseExcuteNonQueryForDelete(string query)
        {
            try
            {
                if (con.State.ToString() == con.State.ToString())
                {
                    con.Close();
                }
                con.Open();
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();
                //con.Close();
            }
            catch
            {


            }

        }

        public void CloseConnection()
        {
            if (con.State.ToString() == con.State.ToString())
            {
                con.Close();
            }
        }

    }
}
