﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RetailsShop.BL;
using RetailsShop.PL;
using RetailsShop.DL;

using System.Data;
using System.Data.SqlClient;


namespace RetailsShop.DL
{
    class DL_CreateUser
    {
        DL_Connection dl_connection = new DL_Connection();


        public DataTable SelectCreateUser()
        {
            string str = "Select c.CreateUserId,s.FirstName,c.UserName,c.Password,c.UserRole,c.Status From CreateUser c inner join StaffRegistration s on c.staffId=s.StaffId";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


        public void InsertCreateUser(BL_Field bl_field)
        {
            string str = "Insert into CreateUser (StaffId,UserName,Password,UserRole,Status) values('" + bl_field.WorkerId + "','" + bl_field.Username + "','" + bl_field.Password + "','" + bl_field.Role + "','" + bl_field.Status + "')";
            dl_connection.UseExcuteNonQuery(str);
        }


        public void UpdateCreateUser(BL_Field bl_field)
        {
            string str = "Update CreateUser set StaffId='" + bl_field.WorkerId + "', UserName='" + bl_field.Username + "',Password='" + bl_field.Password + "',UserRole='" + bl_field.Role + "',Status='" + bl_field.Status + "' where CreateUserId='" + bl_field.WorkerId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void DeleteCreateUser(BL_Field bl_field)
        {
            string str = "Delete from CreateUser  where CreateUserId='" + bl_field.WorkerId + "' ";
            dl_connection.UseExcuteNonQuery(str);
        }



        //=============LogIn==============


         public SqlDataReader SelectLogIn(BL_Field bl_field)
        {
            string str = "Select UserName,Password from CreateUser where UserName='" + bl_field.Username + "' and Password='" + bl_field.Password + "'";
            SqlDataReader dr = dl_connection.UseExcuteDataReader(str);
            return dr;

        }

        //=============Staff==============

         public DataTable SelectStaff()
         {
             string str = "Select StaffId,FirstName from StaffRegistration ";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;

         }
    }
}