﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using RetailsShop.BL;
using RetailsShop.PL;
using RetailsShop.DL;

using System.Data;
using System.Data.SqlClient;



namespace RetailsShop.DL
{
    class DL_AddPaymentMode
    {

        DL_Connection dl_connection = new DL_Connection();
        BL_Field bl_field = new BL_Field();


        public DataTable SelectPaymentMode()
        {
            String str = "Select * from PaymentMode";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }


        public void InsertPaymentMode(BL_Field bl_field)
        {

            string str = "Insert into PaymentMode (PaymentMode)values('" + bl_field.PaymentMode+ "')";
            dl_connection.UseExcuteNonQuery(str);

        }


        public void UpdatePaymentMode(BL_Field bl_field)
        {
            string str = "Update PaymentMode set PaymentMode='" + bl_field.PaymentMode + "' where PaymentModeId='" + bl_field.PaymentModeId + "' ";
            dl_connection.UseExcuteNonQuery(str);

        }

        public void DeletePaymentMode(BL_Field bl_field)
        {
            string str = "Delete from PaymentMode where PaymentModeId='" + bl_field.PaymentModeId + "'";
            dl_connection.UseExcuteNonQuery(str);

        }


    }
}
