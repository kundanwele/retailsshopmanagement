﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;

namespace RetailsShop.DL
{
    class DL_Vendor
    {
        DL_Connection dl_connection = new DL_Connection();

        private const string VendorId = "SupplierId";
        private const string VendorName = "SupplierName";
        private const string ContactPerson = "ContactPerson";
        private const string Country = "Country";
        private const string State = "State";
        private const string City = "City";
        private const string Address = "Address";
        private const string PhoneNo = "PhoneNo";
        private const string MobileNo = "MobileNo";
        private const string Email = "Email";
        private const string Note = "Note";
        private const string Status = "Status";
        private const string tblVendor = "Supplier";


        public void InsertVendor(BL_Vendor bl_vender)
        {
            string str = "insert into " + tblVendor + "(" + VendorName + "," + ContactPerson + "," + Country + "," + State + "," + City + "," + Address + "," + PhoneNo + "," + MobileNo + "," + Email + "," + Note + ")values('" + bl_vender.SupplierName + "','" + bl_vender.ContactPerson + "','" + bl_vender.Country + "','" + bl_vender.State + "','" + bl_vender.City + "','" + bl_vender.Address + "','" + bl_vender.PhoneNo + "','" + bl_vender.Mobile + "','" + bl_vender.Email + "','" + bl_vender.Note + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectVendor()
        {
            string str = "select * from " + tblVendor + "";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SearchVendor(BL_Vendor bl_vender)
        {
            string str = "select * from " + tblVendor + " where " + VendorName + " like '" + bl_vender.TextSearch + "%'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public void UpdateVendor(BL_Vendor bl_vender)
        {
            string str = "update " + tblVendor + " set " + VendorName + "='" + bl_vender.SupplierName + "'," + ContactPerson + "='" + bl_vender.ContactPerson + "'," + Country + "='" + bl_vender.Country + "'," + State + "='" + bl_vender.State + "'," + City + "='" + bl_vender.City + "'," + Address + "='" + bl_vender.Address + "'," + PhoneNo + "='" + bl_vender.PhoneNo + "'," + MobileNo + "='" + bl_vender.Mobile + "'," + Email + "='" + bl_vender.Email + "'," + Note + "='" + bl_vender.Note + "' where " + VendorId + "='" + bl_vender.SupplierId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteVendor(BL_Vendor bl_vender)
        {
            string str = "delete from " + tblVendor + " where " + VendorId + "='" + bl_vender.SupplierId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }


    }
}
