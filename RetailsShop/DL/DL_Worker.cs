﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;


namespace RetailsShop.DL

{
    class DL_Worker
    {
        DL_Connection dl_connection = new DL_Connection();

        private const string WorkerId = "WorkerId";
        private const string Name = "Name";
        private const string MiddleName = "MiddleName";

        private const string LastName = "LastName";
        private const string PhoneNo = "PhoneNo";
        private const string Mobile = "Mobile";
        private const string Email = "Email";

        private const string Country = "Country";
        private const string State = "State";
        private const string City = "City";
        private const string Address = "Address";
        private const string Role = "Role";

        private const string Username = "Username";
        private const string Password = "Password";

        private const string tblWorker = "Worker";
        //catagory
        public void InsertWorker(BL_Field bl_field)
        {
            string str = "insert into " + tblWorker + "(" + Name + "," + MiddleName + "," + LastName + "," + PhoneNo + "," + Mobile + "," + Email + "," + Country + "," + State + "," + City + "," + Address + "," + Role + ","+Username +","+Password +")values('" + bl_field.Name + "','" + bl_field.MiddleName + "','" + bl_field.LastName + "','" + bl_field.PhoneNo + "','" + bl_field.Mobile + "','" + bl_field.Email + "','" + bl_field.Country + "','" + bl_field.State + "','" + bl_field.City + "','" + bl_field.Address + "','" + bl_field.Role + "','"+bl_field .Username +"','"+bl_field .Password +"')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectWorker()
        {
            string str = "select * from " + tblWorker + "";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectWaiter(BL_Field bl_field)
        {
            string str = "select * from " + tblWorker + " where Role='Waiter' and Name like '"+bl_field.TextSearch+"%'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }
        public DataTable SelectWaiterName(BL_Field bl_field)
        {
            string str = "select Name from " + tblWorker + " where Role='Waiter' and Name like '" + bl_field.TextSearch + "%'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }
        public DataTable SelectCashier()
        {
            string str = "select * from " + tblWorker + "";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable CheckUsername(BL_Field bl_field)
        {
            string str = "select "+Username +" from " + tblWorker + " where "+ Username +"='"+ bl_field .Username +"'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        
        public void UpdateWorker(BL_Field bl_field)
        {
            string str = "update " + tblWorker + " set " + Name + "='" + bl_field.Name + "'," + MiddleName + "='" + bl_field.MiddleName + "'," + LastName + "='" + bl_field.LastName + "'," + PhoneNo + "='" + bl_field.PhoneNo + "'," + Mobile + "='" + bl_field.Mobile + "'," + Email + "='" + bl_field.Email + "'," + Country + "='" + bl_field.Country + "'," + State + "='" + bl_field.State + "'," + City + "='" + bl_field.City + "'," + Address + "='" + bl_field.Address + "'," + Role + "='" + bl_field.Role + "',"+Username +"='"+bl_field .Username +"',"+Password +"='"+bl_field .Password +"' where " + WorkerId + "='" + bl_field.WorkerId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteWorker(BL_Field bl_field)
        {
            string str = "delete from " + tblWorker + " where " + WorkerId + "='" + bl_field.WorkerId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public SqlDataReader CheckLogin(BL_Field bl_field)
        {
            string str = "select Username,Password,Role,WorkerId from Worker where Username='" + bl_field.Username + "' and Password='" + bl_field.Password + "'";
          SqlDataReader dr=  dl_connection.UseExcuteDataReader(str);
          return dr;
        }

        public SqlDataReader CheckSecurity(BL_Field bl_field)
        {
            string str = "select Role from Worker where Password='" + bl_field.Password + "'";
            SqlDataReader dr = dl_connection.UseExcuteDataReader(str);
            return dr;
        }






    }
}
