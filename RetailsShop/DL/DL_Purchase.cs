﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;

namespace RetailsShop.DL
{

    class DL_Purchase
    {

        DL_Connection dl_connection = new DL_Connection();

      
        
        public void InsertPurchaseDetail(BL_Purchase bl_purchase)
        {
            string str = "insert into PurchaseDetail (PurchaseId,ProductNameId,SerialNo,Qty,Rate,Discount,Status)values('" + bl_purchase.PurchaseId + "','" + bl_purchase.ProductNameId + "','" + bl_purchase.SerialNo + "','" + bl_purchase.Qty + "','" + bl_purchase.Rate + "','" + bl_purchase.Discount + "','" + bl_purchase.Status + "')";
            dl_connection.UseExcuteNonQuery(str);
           
        }
        //public void InsertPurchaseItem(BL_Purchase bl_purchase)
        //{
        //    string str = "insert into Purchase (PurchaseId,EntryDate,ProductNameId,Qty,Size,TabQty,BatchNo,PDate,ExpDate,Rate,TaxPer,TaxAmt,Amount)values('" + bl_purchase.PurchaseId + "','" + bl_purchase.ProductNameId + "','" + bl_purchase.Qty + "','" + bl_purchase.PackSize + "','" + bl_purchase.TabQty + "','" + bl_purchase.BatchNo + "','" + bl_purchase.Date + "','" + bl_purchase.ExpDate + "','" + bl_purchase.Rate + "','" + bl_purchase.Tax + "','" + bl_purchase.TaxAmt + "','" + bl_purchase.Total + "')";
        //    dl_connection.UseExcuteNonQuery(str);
           
        //}
        public DataTable SelectPurchaseDetail(BL_Purchase bl_purchase)
        {
            string str="select p.PurchaseDetailId, p.PurchaseId,p.ProductNameId,p.SerialNo,p.Qty,p.Rate,p.Discount,p.Status from PurchaseDetail p ";          
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public void UpdatePurchaseDetail(BL_Purchase bl_purchase)
        {
            string str = "update PurchaseDetail set ProductNameId='" + bl_purchase.ProductNameId + "',SerialNo='" + bl_purchase.SerialNo + "',Qty ='" + bl_purchase.Qty + "',Rate='" + bl_purchase.Rate + "',Discount='" + bl_purchase.Discount + "',Status='" + bl_purchase.Status + "' where PurchaseDetailId ='" + bl_purchase.PurchaseDetailId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeletePurchaseDetail(BL_Purchase bl_purchase)
        {
            string str = "delete from  PurchaseDetail where PurchaseDetailId='" + bl_purchase.PurchaseDetailId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        //public DataTable SelectPurchaseByDate(BL_Purchase bl_purchase)
        //{
        //    string str = "select * from " + tblPurchase + " where convert(datetime, " + Date + ", 103) between convert(datetime, '" + bl_purchase.FromDate + "', 103) and convert(datetime, '" + bl_purchase.Todate + "', 103)";
        //    DataTable dt = dl_connection.UseDatatable(str);
        //    return dt;
        //}
        //public DataTable SelectProductNameBySearch(BL_Purchase bl_purchase)
        //{
        //    string str = "select pn.ProductNameId,pn.ProductName,c.CompanyName,pn.PackSize,pn.PurchaseSize,pt.ProductName as ProductType,pn.ProductGroup,pn.PurchaseRate,pn.MRP,pn.Tax,pn.Rack,pn.ReorderLevel,pn.Status from ProductName pn inner join Company c on pn.CompanyId=c.TblId inner join ProductType pt on pn.ProductType=pt.ProductId where pn.ProductName like '" + bl_purchase.TextSearch + "%'";
        //    DataTable dt = dl_connection.UseDatatable(str);
        //    return dt;
        //}

        //public void ClosePurchase()
        //{
        //    string str = "update " + tblPurchase + " set Status='Close'";
        //    dl_connection.UseExcuteNonQuery(str);
        //}


        //public DataTable SelectPurchaseById(BL_Purchase bl_purchase)
        //{
        //    string str = "select * from Purchase where ProductNameId='"+bl_purchase.ProductNameId+"'";
        //    DataTable dt = dl_connection.UseDatatable(str);
        //    return dt;
        //}



        public DataTable SelectManufact()
        {
            string str = "select ManufactureName,ManufactureId from Manufacturer ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        //public DataTable SelectManufact()
        //{
        //    string str = "select ManufactureName,ManufactureId from Manufacturer ";
        //    DataTable dt = dl_connection.UseDatatable(str);
        //    return dt;

        //}

        //public string InsertPurchase(BL_Purchase bl_purchase)
        //{
        //    string str = "insert into Purchase (ManufactureId,SupplierId,Date,InvoiceNo,Vat)values('" + bl_purchase.ManufactureId + "','" + bl_purchase.SupplierId + "','" + bl_purchase.Date + "','" + bl_purchase.InvoiceNo + "','" + bl_purchase.Rate + "','" + bl_purchase.Vat + "')";
        //    string id = dl_connection.UseExcuteScaler(str);
        //    return id;
        //}

        public void InsertPurchase(BL_Purchase bl_purchase)
        {
            string str = "insert into Purchase (ManufactureId,SupplierId,Date,InvoiceNo,Vat)values('" + bl_purchase.ManufactureId + "','" + bl_purchase.SupplierId + "','" + bl_purchase.Date + "','" + bl_purchase.InvoiceNo + "','" + bl_purchase.Vat + "')";
            dl_connection.UseExcuteNonQuery(str);
            
        }


        public DataTable SelectProduct(BL_Purchase bl_purchase)
        {
            string str = "select ProductNameId,ProductName,PurchaseQty,PurchaseRate,SaleRate from AddProduct where ProductName like '" + bl_purchase.ProductName + "%' ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public DataTable SelectAllProduct()
        {
            string str = "select ProductNameId,ProductName,PurchaseQty,PurchaseRate,SaleRate from AddProduct  ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }



    }
}
