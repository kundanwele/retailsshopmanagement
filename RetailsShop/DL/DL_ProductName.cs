﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;

namespace RetailsShop.DL
{
    class DL_ProductName
    {

        DL_Connection dl_connection = new DL_Connection();
       
        

        //producname
        public void InsertProductName(BL_ProductName bl_productname)
        {
            string str = "insert into AddProduct(ProductName,ManufactureId,PurchaseQty,PurchaseRate,SaleRate,Reorderlevel)values('" + bl_productname.ProductName + "','" + bl_productname.ManufactureId + "','" + bl_productname.PurchaseQty + "','" + bl_productname.PurchaseRate + "','" + bl_productname.SaleRate + "','" + bl_productname.Reorderlevel + "')";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable SelectProductName()
        {
            string str = "Select pn.ProductNameId, pn.ProductName,m.ManufactureName,pn.PurchaseQty,pn.PurchaseRate,pn.SaleRate,pn.ReorderLevel from AddProduct pn inner join Manufacturer m on pn.ManufactureId=m.ManufactureId ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public void UpdateProductName(BL_ProductName bl_productname)
        {
            string str = "update AddProduct set ProductName='" + bl_productname.ProductName + "',ManufactureId='" + bl_productname.ManufactureId + "',PurchaseRate='" + bl_productname.PurchaseRate + "',PurchaseQty='" + bl_productname.PurchaseQty + "',SaleRate='" + bl_productname.SaleRate + "',Reorderlevel='" + bl_productname.Reorderlevel + "' where ProductNameId='" + bl_productname.ProductNameId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteProductName(BL_ProductName bl_productname)
        {
            string str = "delete from AddProduct where ProductNameId='" + bl_productname.ProductNameId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        //public DataTable SelectProductNametoSale(BL_ProductName bl_productname)
        //{
        //    string str = "select pn.ProductName,pn.MRP,c.CompanyName,pn.Rack,p.*,pn.CompanyId,pn.PurchaseSize from Purchase p inner join ProductName pn on p.ProductNameId=pn.ProductNameId inner join Company c on pn.CompanyId=c.TblId where pn.ProductName like'" + bl_productname.TextSearch + "%'";
        //    DataTable dt = dl_connection.UseDatatable(str);
        //    return dt;

        //}

        public DataTable SelectManufact()
        {
            string str = "select ManufactureName,ManufactureId from Manufacturer ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
 
        }

        public DataTable SelectProductNameBySearch(BL_ProductName bl_productname)
        {
            string str = "select p.ProductNameId,p.ProductName,m.ManufactureName,p.SaleRate from AddProduct p inner join Manufacturer m on p.ManufactureId=m.ManufactureId where p.ProductName like '" + bl_productname.TextSearch + "%'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


    }

}
