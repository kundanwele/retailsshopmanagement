﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;

namespace RetailsShop.DL
{
    class DL_Manufacturer
    {


        DL_Connection dl_connection = new DL_Connection();


        public void InsertManufacturer(BL_Manufacturer bl_manufacture)
        {
            string str = "insert into Manufacturer(ManufactureName,ManufactureDiscri)values('" + bl_manufacture.ManufactureName + "','" + bl_manufacture.ManufactureDiscri + "')";
            dl_connection.UseExcuteNonQuery(str);
        }


        public DataTable SelectManufacturer()
        {
            string str = "select * from Manufacturer";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }


        public void UpdateManufacturer(BL_Manufacturer bl_manufacture)
        {
            string str = "update Manufacturer set ManufactureName ='" + bl_manufacture.ManufactureName + "', ManufactureDiscri ='" + bl_manufacture.ManufactureDiscri + "' where ManufactureId='" + bl_manufacture.ManufactureId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }



        public void DeleteManufacturer(BL_Manufacturer bl_manufacture)
        {
            string str = "delete from Manufacturer where ManufactureId='" + bl_manufacture.ManufactureId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }


    }
}
