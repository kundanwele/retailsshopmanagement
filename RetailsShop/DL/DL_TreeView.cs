﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using RetailsShop.DL;
using RetailsShop.BL;
using RetailsShop.PL;
using System.Data;


namespace RetailsShop.DL
{
    class DL_TreeView
    {

        DL_Connection dl_connection = new DL_Connection();

        //catagory
        public void InsertTreeViewMenu(BL_TreeView bl_treeview)
        {
            string str = "insert into TreeViewMenu(MenuName)values('" + bl_treeview.MenuName + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectTreeViewMenu(BL_TreeView bl_treeview)
        {
            string str = "select * from TreeViewMenu";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }


        public void UpdateTreeViewMenu(BL_TreeView bl_treeview)
        {
            string str = "update TreeViewMenu set MenuName='" + bl_treeview.MenuName + "' where MenuId='" + bl_treeview.MenuId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteTreeViewMenu(BL_TreeView bl_treeview)
        {
            string str = "delete from TreeViewMenu where MenuId='" + bl_treeview.MenuId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

      
        //item

        public void InsertTreeViewItem(BL_TreeView bl_treeview)
        {
            string str = "insert into TreeViewItem(MenuId,ItemName)values('" + bl_treeview.MenuId + "','" + bl_treeview.ItemName + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectTreeViewItem(string MenuId)
        {
            string str = "select * from TreeViewItem where MenuId='" + MenuId + "' order by ItemName asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


        public void UpdateTreeViewItem(BL_TreeView bl_treeview)
        {
            string str = "update TreeViewItem set MenuId='" + bl_treeview.MenuId + "',ItemName='" + bl_treeview.ItemName + "' where ItemId='" + bl_treeview.ItemId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteTreeViewItem(BL_TreeView bl_treeview)
        {
            string str = "delete from TreeViewItem where ItemId='" + bl_treeview.ItemId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void TreeViewItemChange(BL_TreeView bl_treeview)
        {
            string str = "update TreeViewItem set Status='"+bl_treeview.Status+"' where ItemName='"+bl_treeview.ItemName+"'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable SelectTreeViewItem()
        {
            string str = "select * from TreeViewItem";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

    }
}
